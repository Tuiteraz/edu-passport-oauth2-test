#--- REQUIRE --------------
#--packages
_    = require "underscore"
Path = require 'path'


exports.settings =
  sVersion: '0.0.1'
  sTitle: "Passport-OAuth2 test"
  iPort: 3335
  sRootDir: ""
  sProductionAppFile: "app.js"
  aErrorTitles : {
    404: "Not found"
    500: "Internal server error"
  }
  iUrlListMaxLength: 500
  sSiteHtmlSnapshotsPath: "/snapshots"
  cp:
    sSourcePath : Path.join( process.cwd(),"app/cp" )
    models:
      hRefSites:
        sCollectionName: "ref_site"


