#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
mongoose       = require('mongoose')

#--) DEPENDENCIES

AuthCodeSchema = new mongoose.Schema {
  userId:
    type: String
    required: true
  clientId:
    type: String
    required: true
  code:
    type: String
    unique: true
    required: true
  redirectURI:
    type: String
    required: true
}


mongoose.model 'auth_code', AuthCodeSchema