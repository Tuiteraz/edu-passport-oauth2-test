#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
mongoose       = require('mongoose')

#--) DEPENDENCIES

RefreshTokenSchema = new mongoose.Schema {
  userId:
    type: String
    required: true
  clientId:
    type: String
    required: true
  token:
    type: String
    unique: true
    required: true
  created:
    type: Date
    default: Date.now
}


mongoose.model 'refresh_token', RefreshTokenSchema