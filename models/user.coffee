#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
mongoose       = require('mongoose')
_              = require "underscore"
_.str          = require "underscore.string"
_.mixin _.str.exports()

bcrypt         = require "bcrypt"

#--) DEPENDENCIES

#--( PREPARE

SALT_WORK_FACTOR = 10

#--) PREPARE

UserSchema = new mongoose.Schema {
  username:
    type: String
    unique: true
    required: true
  password:
    type: String
    required: true
}

UserSchema.methods.comparePassword = (passwordCandidate,callback)->
  bcrypt.compare passwordCandidate, @password, (err, isMatch)->
    return callback(err) if err

    callback null, isMatch

UserSchema.pre "save", (next)->
  user = this
  return next() if !user.isModified "password"

  bcrypt.genSalt SALT_WORK_FACTOR, (err,salt)->
    return next(err) if err

    bcrypt.hash user.password, salt, (err,hash)->
      return next(err) if err
      user.password = hash
      next()

UserSchema.virtual('userId').get ()->
  @id

mongoose.model 'user', UserSchema