#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
config         = require('nconf')

_              = require "underscore"
_.str          = require "underscore.string"

_.mixin _.str.exports()

#--) DEPENDENCIES

#--( PREPARE

clients = [
  {
    id           : '1'
    name         : 'IFTTT'
    clientId     : config.get('ifttt:clientId')
    clientSecret : config.get('ifttt:clientSecret')
  }
]

#--) PREPARE

exports.find = (id,done)->
  client = _.findWhere clients, {id:id}
  done null, client

exports.findByClientId = (clientId,done)->
  client = _.findWhere clients, {clientId:clientId}
  done null, client

