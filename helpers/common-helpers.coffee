#--- REQUIRE --------------
#--packages
Swig           = require 'swig'
Path           = require 'path'
_              = require "underscore"
_.str          = require "underscore.string"
_.mixin _.str.exports()

bcrypt         = require "bcrypt"

#--modules
Settings         = require("../settings").settings

#--- PREPARE --------------

log = _.bind console.log,console


#--- BODY --------------

# parse http[s]://test.wtrmln.org:3000 -> test
# DON'T parse http[s]://WWW.test.wtrmln.org:3000 -> test
# +2013.7.25 tuiteraz
get_subdomain_of= (sHost) ->
  sRegEx = /(?:http[s]*\:\/\/)*(.*?)\.(?=[^\/]*\..{2,5})/i
  aMatches = sHost.match sRegEx
  sRes = if !_.isNull aMatches then aMatches[1] else ""

  return sRes

# +2013.7.25 tuiteraz
# *2013.9.13 tuiteraz: +@ symbol for image name
# *2013.9.24 tuiteraz: адреса типа /news/111111 понимались как файлы - добавил расширение в конце
is_filename= (sSample) ->
  sRegEx = /^(\/?[a-zA-Z-_0-9]+){0,10}(?:\/)([\w\.\-@^\s]+\.[A-Za-z]{2,6})$/i
  return sRegEx.test sSample

#+2013.9.4 tuiteraz
#*2013.9.20 tuiteraz: +Swig tmpl processing
#*2013.10.31 tuiteraz: +bPlain=false
error=(iError, jResponse, sText,bPlain=false) ->
#  log "[#{iError}] : #{sText}"

  if !bPlain
    jTmpl = Swig.compileFile Path.join(process.cwd(),"/app/views/error.html")
    sHtml = jTmpl {
      sText: sText
      iError: iError
      sErrorTitle : Settings.aErrorTitles[iError]
      sServer : "#{Settings.sTitle} #{Settings.sVersion}"
    }

    jResponse.writeHead iError, {"Content-Type": "text/html"}
    jResponse.end sHtml
  else
    jResponse.writeHead iError, {"Content-Type": "text/plain"}
    jResponse.end sText

#+2013.11.29 tuiteraz
is_cp_request= (jRequest) ->
  return /^https?\:\/\/[\w.-]*\/cp\/(?:[\w\W]*)(?:#!)?$/i.test jRequest.headers.referer

#+2013.10.3 tuiteraz
is_tool_path = (sUri) ->
  sToolName = _.find Settings.tools.aNames, (sToolName) ->
    jRegExp = new RegExp("^\/#{sToolName}$","i")
    jRegExp.test sUri
  if !_.isEmpty(sToolName)
    return true
  else
    return false

#+2014.3.12 tuiteraz
is_auth_tool_path = (sUri) ->
  sToolName = _.find Settings.tools.aNames, (sToolName) ->
    jRegExp = new RegExp("^\/#{sToolName}$","i")
    jRegExp.test sUri
  if sToolName == "auth"
    return true
  else
    return false


#+2013.10.3 tuiteraz
is_db_path = (sUri) ->
  jRegExp = new RegExp("^\/#{Settings.mongo.sUriPath}","i")

  if jRegExp.test sUri
    return true
  else
    return false

# scan for key ref:'' deep inside hash and returns arrays of referenced schemas names
#+2013.11.9 tuiteraz
get_referenced_schemas = (Object,aRes=null) ->
  aRes ||= []
  if _.isObject(Object)
    for sKey, Value of Object
      if sKey =='ref'
        aRes.push Value
      else if _.isObject(Value) || _.isArray(Value)
        get_referenced_schemas Value,aRes
  else if _.isArray(Object)
    for Value in Object
      get_referenced_schemas Value,aRes if _.isObject(Value) || _.isArray(Value)

  return aRes

# пока не используем
#+2013.11.10 tuiteraz
get_creator_collection_name = (Mongoose, sCollectionName) ->
  mSchema = Mongoose.modelSchemas[sCollectionName]
  sCreatorCollName = mSchema.tree._creator.ref
  return sCreatorCollName

# пока не используем
# у владельца может быть только одно поле ссылающееся на подчиненного
#+2013.11.10 tuiteraz
get_binded_property_params = (Mongoose,sCreatorCollName,sBindedCollName) ->
  mCreatorSchema = Mongoose.modelSchemas[sCreatorCollName]
  hRes = {}
  for sPropertyName, Type of mCreatorSchema.tree
    if _.isArray Type
      for hBinding in Type
        if hBinding.ref == sBindedCollName
          hRes =
            sPropertyName : sPropertyName
            bIsArray : true

#+2013.11.21 tuiteraz
write_response = (iStatus,Res,jResponse,bNeedStringify=true,sContentType="text/json") ->
  jResponse.writeHead iStatus, {"Content-Type": sContentType}
  if bNeedStringify
    jResponse.end JSON.stringify Res
  else
    jResponse.end Res


#---------------

exports.get_subdomain_of        = get_subdomain_of
exports.is_cp_request           = is_cp_request
exports.is_filename             = is_filename
exports.is_tool_path            = is_tool_path
exports.is_auth_tool_path       = is_auth_tool_path
exports.is_db_path              = is_db_path
exports.error                   = error
exports.get_referenced_schemas  = get_referenced_schemas
exports.write_response          = write_response


