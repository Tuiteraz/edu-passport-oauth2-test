#--( DEPENDENCIES
config      = require('nconf')
_           = require "underscore"
_.str       = require "underscore.string"
_.mixin _.str.exports()

fs          = require "fs"
path        = require 'path'

hlprs       = require "./../helpers/common-helpers"

#--) DEPENDENCIES

#+2013.12.5 tuiteraz
#*2014.2.20 tuiteraz: replace sFileName & sFileData -> aFiles[{sFilename,sData}]
isReqBodyIsFile=(jRequest) ->
  return _.has(jRequest.body, "aFiles")

#+2013.12.5 tuiteraz
parseBodyFiles=(jRequest)->
  if isReqBodyIsFile(jRequest)
    aFiles = []
    for hFile in jRequest.body.aFiles
      sMime   = hFile.sData.match(/:([\w\/]+);/i)[1]
      sData   = _.strRight hFile.sData, ","
      jBuffer = new Buffer(sData, 'base64')
      aFiles.push {
        name   : hFile.sName
        data   : jBuffer.toString()
        buffer : jBuffer
        mime   : sMime
      }

    return aFiles
  else
    return null

#+2013.12.5 tuiteraz
checkFilenameForDir= (sDestDir, sOldName)->
  bExists = true
  aAllFileNames = while bExists
    if iVerNumber
      sNewName = "#{sOldName}.#{iVerNumber}"
    else
      sNewName = sOldName
      iVerNumber = 0

    sTmpFilepath = path.join sDestDir, sNewName
    if fs.existsSync(sTmpFilepath)
      iVerNumber += 1
    else
      bExists = false
    sNewName

  _.last aAllFileNames

exports.isReqBodyIsFile     = isReqBodyIsFile
exports.parseBodyFiles      = parseBodyFiles
exports.checkFilenameForDir = checkFilenameForDir


