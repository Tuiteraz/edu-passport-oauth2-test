// Generated by CoffeeScript 1.6.3
(function() {
  define(['beaver-console'], function(jconsole) {
    jconsole.info("nav-item-helpers");
    return {
      init: function(ctrlParent) {
        this.ctrlParent = ctrlParent;
        return this.hCfgSite = this.ctrlParent.hCfgSite;
      },
      extra_prop_value_of: function(hNavItem, sExtraPropName) {
        var Res;
        Res = null;
        if (defined(hNavItem.aExtraProperties)) {
          if (_.isObject(hNavItem.aExtraProperties)) {
            if (!_.isUndefined(hNavItem.aExtraProperties[sExtraPropName])) {
              Res = hNavItem.aExtraProperties[sExtraPropName];
            }
          }
        }
        return Res;
      },
      size_class_of: function(hNavItem) {
        var iSize, sRes;
        iSize = this.extra_prop_value_of(hNavItem, "iTitleTextSize");
        sRes = "";
        if (!_.isNull(iSize)) {
          sRes = "size" + iSize;
        }
        return sRes;
      },
      is_empty: function(hNavItem) {
        var bNoCats, bNoPage, bNoTags, bRes;
        bNoPage = _.isUndefined(hNavItem.id) || _.isEmpty(hNavItem.id) ? true : false;
        bNoTags = _.isUndefined(hNavItem.aPostTags) || _.size(hNavItem.aPostTags) === 0 ? true : false;
        bNoCats = _.isUndefined(hNavItem.aPostCategories) || _.size(hNavItem.aPostCategories) === 0 ? true : false;
        if (!_.isUndefined(hNavItem.bEmpty)) {
          bRes = hNavItem.bEmpty;
        } else {
          bRes = bNoPage && bNoTags && bNoCats ? true : false;
        }
        return bRes;
      },
      is_group: function(hNavItem) {
        var bRes;
        bRes = false;
        if (!_.isUndefined(hNavItem.aChildrens)) {
          if (_.size(hNavItem.aChildrens) > 0) {
            bRes = true;
          }
        }
        return bRes;
      },
      is_page: function(hNI) {
        return hNI.optNavType === "page";
      },
      is_showroom: function(hNI) {
        return hNI.optNavType === "showroom";
      },
      is_catalog: function(hNI) {
        return hNI.optNavType === "catalog";
      },
      detect_by_slug: function(aNavItems, sNavSlug) {
        var hItem, hNavItem, hSubItem, _i, _j, _len, _len1, _ref;
        hNavItem = void 0;
        for (_i = 0, _len = aNavItems.length; _i < _len; _i++) {
          hItem = aNavItems[_i];
          if (hItem.sSlug === sNavSlug) {
            hNavItem = hItem;
            break;
          } else {
            if (_.isArray(hItem.aChildrens)) {
              _ref = hItem.aChildrens;
              for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
                hSubItem = _ref[_j];
                if (hSubItem.sSlug === sNavSlug) {
                  hNavItem = hSubItem;
                  break;
                }
              }
              if (!_.isUndefined(hNavItem)) {
                break;
              }
            }
          }
        }
        return hNavItem;
      },
      detect_by_attr: function(aNavItems, hAttr) {
        var hItem, hNavItem, _i, _len;
        hNavItem = _.findWhere(aNavItems, hAttr);
        if (!hNavItem) {
          hNavItem = void 0;
          for (_i = 0, _len = aNavItems.length; _i < _len; _i++) {
            hItem = aNavItems[_i];
            if (_.isArray(hItem.aChildrens)) {
              hNavItem = _.findWhere(hItem.aChildrens, hAttr);
              if (!_.isUndefined(hNavItem)) {
                break;
              }
            }
          }
        }
        return hNavItem;
      },
      detect_by_href: function(aNavItems, sHref) {
        var aSlugs, hNavItem, sLogHeader;
        sLogHeader = "detect_by_href() : ";
        hNavItem = void 0;
        aSlugs = this.extract_slug_from_hash(sHref);
        hNavItem = this.detect_by_slug(aNavItems, _.last(aSlugs));
        return hNavItem;
      },
      extract_slug_from_hash: function(sHash) {
        var aChunks, aRes, iLastIdx, sChunk, sDecodedHash, _i, _len;
        sDecodedHash = decodeURIComponent(sHash);
        aChunks = sDecodedHash.split('/');
        iLastIdx = _.size(aChunks) - 1;
        aChunks[iLastIdx] = _(aChunks[iLastIdx]).strLeft(this.hCfgSite.sUrlHashToken);
        aRes = [];
        for (_i = 0, _len = aChunks.length; _i < _len; _i++) {
          sChunk = aChunks[_i];
          if (!_.isEmpty(sChunk)) {
            if (!_(sChunk).include('#')) {
              aRes.push(sChunk);
            }
          }
        }
        return aRes;
      },
      get_href_for: function(hNavItem, sParams) {
        var aHash, sHash, sItemToken, sLocale, sParamsRef, sRes;
        if (sParams == null) {
          sParams = null;
        }
        if (sParams) {
          sParamsRef = sParams;
        } else {
          sParamsRef = "";
        }
        if (hNavItem.sSlug) {
          sItemToken = hNavItem.sSlug;
        } else if (hNavItem._id) {
          sItemToken = hNavItem._id;
        } else if (hNavItem.id) {
          sItemToken = hNavItem.id;
        } else {
          sItemToken = hNavItem.i18nTitle;
        }
        if (defined(SITE.sCurrLocaleSlug)) {
          sLocale = SITE.sCurrLocaleSlug;
        } else {
          sHash = History.getState().hash;
          aHash = sHash.split("/");
          if (aHash[1].length === 2) {
            sLocale = aHash[1];
          } else {
            sLocale = 'unkown';
          }
        }
        sRes = "/" + sLocale + "/" + sItemToken + this.hCfgSite.sUrlHashToken + sParamsRef;
        return sRes;
      }
    };
  });

}).call(this);

/*
//@ sourceMappingURL=nav-item-helpers.map
*/
