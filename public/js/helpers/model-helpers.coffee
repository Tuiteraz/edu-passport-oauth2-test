define [
  'beaver-console'
  "configuration/site-cfg"
  "models/ref-locale-model"
  "models/ref-coin-model"
  "models/ref-coin-collection-model"
  "models/ref-country-model"
  "models/ref-currency-model"
  "models/ref-metal-model"
  "models/ref-quality-model"
  "models/ref-shape-model"
  "models/ref-extra-model"
  "models/ref-file-model"
  "models/ref-page-model"
], (
  jconsole
  cfgSite
  RefLocaleModel
  RefCoinModel
  RefCoinCollectionModel
  RefCountryModel
  RefCurrencyModel
  RefMetalModel
  RefQualityModel
  RefShapeModel
  RefExtraModel
  RefFileModel
  RefPageModel
) ->
  jconsole.info "cp-helpers"

  #+2014.2.19 tuiteraz
  get_for_collection_name: (sCollectionName)->
    hControl =
      hDataBinding :
        sCollectionName : sCollectionName

    @get_for_content_control hControl

  #+2014.1.30 tuiteraz
  get_for_content_control:(hControl)->
    @hCfgSite ||= cfgSite.get_content()

    sCollectionName = hControl.hDataBinding.sCollectionName
    jconsole.log "model-helpers.get_for_content_control(#{sCollectionName})"

    if sCollectionName      == "ref_locale"
      mRes  = new RefLocaleModel @hCfgSite.hMongoDB.hModels.hRefLocale
    else if sCollectionName == "ref_coin_collection"
      mRes  = new RefCoinCollectionModel @hCfgSite.hMongoDB.hModels.hRefCoinCollection
    else if sCollectionName == "ref_country"
      mRes  = new RefCountryModel @hCfgSite.hMongoDB.hModels.hRefCountry
    else if sCollectionName == "ref_currency"
      mRes  = new RefCurrencyModel @hCfgSite.hMongoDB.hModels.hRefCurrency
    else if sCollectionName == "ref_metal"
      mRes  = new RefMetalModel @hCfgSite.hMongoDB.hModels.hRefMetal
    else if sCollectionName == "ref_quality"
      mRes  = new RefQualityModel @hCfgSite.hMongoDB.hModels.hRefQuality
    else if sCollectionName == "ref_shape"
      mRes  = new RefShapeModel @hCfgSite.hMongoDB.hModels.hRefShape
    else if sCollectionName == "ref_extra"
      mRes  = new RefExtraModel @hCfgSite.hMongoDB.hModels.hRefExtra
    else if sCollectionName == "ref_edge"
      mRes  = new RefExtraModel @hCfgSite.hMongoDB.hModels.hRefEdge
    else if sCollectionName == "ref_coin_availability"
      mRes  = new RefExtraModel @hCfgSite.hMongoDB.hModels.hRefCoinAvailability
    else if sCollectionName == "ref_coin_status"
      mRes  = new RefExtraModel @hCfgSite.hMongoDB.hModels.hRefCoinStatus
    else if sCollectionName == "ref_file"
      mRes  = new RefFileModel @hCfgSite.hMongoDB.hModels.hRefFile
    else if sCollectionName == "ref_page"
      mRes  = new RefPageModel @hCfgSite.hMongoDB.hModels.hRefPage
    else if sCollectionName == "ref_coin"
      mRes  = new RefCoinModel @hCfgSite.hMongoDB.hModels.hRefCoin
    else if sCollectionName == "ref_text_snippet"
      mRes  = new RefCoinModel @hCfgSite.hMongoDB.hModels.hRefTextSnippet

    return mRes

  # check sItemId for using in collection's property
  #+2014.2.26 tuiteraz
  get_id_usage:(sItemId,sCollectionName,sPropertyName, fnCallback)->
    jconsole.log "get_id_usage()"

    hQuery = {}
    hQuery[sPropertyName] = sItemId

    mCollection = @get_for_collection_name(sCollectionName)
    mCollection.get hQuery,{}, fnCallback
