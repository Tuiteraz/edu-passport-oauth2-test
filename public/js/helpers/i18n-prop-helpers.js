// Generated by CoffeeScript 1.6.3
(function() {
  define(['beaver-console'], function(jconsole) {
    jconsole.info("i18n-prop-helpers");
    return {
      flatten: function(hItem, sLocaleSlug) {
        var aRes, hItem1, _i, _len,
          _this = this;
        if (_.isArray(hItem)) {
          aRes = [];
          for (_i = 0, _len = hItem.length; _i < _len; _i++) {
            hItem1 = hItem[_i];
            aRes.push(this.flatten(hItem1, sLocaleSlug));
          }
          return aRes;
        } else {
          _.each(hItem, function(Value, sPropName) {
            if (/^i18n[a-z\d_]+$/i.test(sPropName)) {
              return hItem[sPropName] = hItem[sPropName][sLocaleSlug];
            } else if (_.isArray(hItem[sPropName])) {
              return _.each(hItem[sPropName], function(hArrayItem, iIdx) {
                var _this = this;
                return _.each(hArrayItem, function(Value, sPropName2) {
                  if (/^i18n[a-z\d_]+$/i.test(sPropName2)) {
                    return hItem[sPropName][iIdx][sPropName2] = hItem[sPropName][iIdx][sPropName2][sLocaleSlug];
                  }
                });
              });
            } else if (_.isObject(hItem[sPropName])) {
              return hItem[sPropName] = _this.flatten(hItem[sPropName], sLocaleSlug);
            }
          });
          return hItem;
        }
      },
      get_list: function(hData) {
        var aPropNames, aRes,
          _this = this;
        aPropNames = _.keys(hData);
        aRes = [];
        _.each(aPropNames, function(sPropName) {
          var aPropRes;
          if (/^i18n[a-z\d_]+$/i.test(sPropName)) {
            return aRes.push(sPropName);
          } else {
            if (_.isObject(hData[sPropName])) {
              aPropRes = _this.get_list(hData[sPropName]);
              return aRes = _.extend(aRes, aPropRes);
            }
          }
        });
        return aRes;
      }
    };
  });

}).call(this);

/*
//@ sourceMappingURL=i18n-prop-helpers.map
*/
