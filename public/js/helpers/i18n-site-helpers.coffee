define [
  'beaver-console'
  'helpers/helpers'
  'helpers/model-helpers'
], (
  jconsole
  hlprs
  model_hlprs
) ->
  jconsole.info "i18n-site-helpers"

  #+2014.2.28 tuiteraz
  init:(@hCfgSettings,fnCallback)->
    @sLogHeader = "helpers/i18n-site-helpers"
    jconsole.log "#{@sLogHeader}.init()"

    window.t = _.bind @get_for_curr_locale_by_slug, {jThis:this}

    mTextSnippets = model_hlprs.get_for_collection_name("ref_text_snippet")
    mTextSnippets.get {},{},(hRes)=>
      hlprs.show_results(hRes) if hRes.iStatus != 200
      @aTextSnippets = hRes.aData
      fnCallback()

    @check_curr_locale_from_hash()

  #+2014.3.1 tuiteraz
  check_curr_locale_from_hash:()->
    if defined SITE.sCurrLocaleSlug
      sLocaleSlug = SITE.sCurrLocaleSlug
    else
      sHash = History.getState().hash
      aHash = sHash.split "/"

      if aHash[1].length == 2
        sLocaleSlug = aHash[1]
        SITE.sCurrLocaleSlug = sLocaleSlug

    return sLocaleSlug

  # common call after _.bind and them this.jContext = 'this' of current i18n-site-hlprs module
  #+2014.2.28 tuiteraz
  get_for_curr_locale_by_slug:(sSlug)->
    sLocaleSlug = this.jThis.check_curr_locale_from_hash()
    hTxtSnippet = _.findWhere this.jThis.aTextSnippets, {sSlug:sSlug}
    if defined hTxtSnippet
      return hTxtSnippet.i18nText[sLocaleSlug]
    else
      # sSlug can be i18nText in default locale
      # so let's extend aTextSnippets to search by def locale i18nText value
      if defined this.jThis.aTextSnippets
        if !defined this.jThis.aTextSnippets[0].sDefLocaleValue
          this.jThis.aTextSnippets = _.map this.jThis.aTextSnippets, (hSnippet)=>
            sDefLocaleSlug = this.jThis.hCfgSettings.appearance.refDefaultLocale.Value
            hSnippet.sDefLocaleValue = hSnippet.i18nText[sDefLocaleSlug]
            return hSnippet

        hTxtSnippet = _.findWhere this.jThis.aTextSnippets, {sDefLocaleValue:sSlug}
        if defined hTxtSnippet
          return hTxtSnippet.i18nText[sLocaleSlug]
        else
          return hTxtSnippet
      else
        return null