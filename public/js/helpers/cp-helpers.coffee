define [
  'beaver-console'
], (
  jconsole
) ->
  jconsole.info "cp-helpers"

  #+2014.1.27 tuiteraz
  init:(ctrlCP) ->
    @hCfgCP = ctrlCP.hCfgCP

  # +2013.11.28 tuiteraz
  detect_section_nav_item_by_slug: (hNavItems,sNavSlug) ->
    hNavItem = undefined
    if _.isObject(hNavItems)
      for sKey,hItem of hNavItems
        if !_.isUndefined(hItem) && !_.isNull(hItem)
          if hItem.sSlug == sNavSlug
            hNavItem = hItem
            break
          else
            #  у группы не должно быть атрибута sSlug
            if _.isObject(hItem) && _.isUndefined(hItem.sSlug)
              hNavItem = @detect_section_nav_item_by_slug(hItem,sNavSlug)
            break if !_.isUndefined hNavItem

    return hNavItem

  # +2013.11.28 tuiteraz
  detect_section_content_control_by_id: (sId,hSectionContent) ->
    hControl = undefined
    if _.isObject(hSectionContent)
      for sKey,hItem of hSectionContent
        if !_.isUndefined(hItem) && !_.isNull(hItem)
          if hItem.id == sId
            hControl = hItem
            hControl._sKey = sKey
            break
          else
            #  у группы не должно быть атрибута 'id'
            if _.isObject(hItem) && _.isUndefined(hItem.id)
              hControl = @detect_section_content_control_by_id(sId,hItem)
            break if !_.isUndefined hControl

    return hControl

  # +2014.2.24 tuiteraz
  detect_section_content_control_by_id_class: (sIdClass,hSectionContent) ->
    hControl = undefined
    if _.isObject(hSectionContent)
      for sKey,hItem of hSectionContent
        if !_.isUndefined(hItem) && !_.isNull(hItem)
          if hItem.sIdClass == sIdClass
            hControl = hItem
            hControl._sKey = sKey
            break
          else
            #  у группы не должно быть атрибута 'id'
            if _.isObject(hItem) && _.isUndefined(hItem.sIdClass)
              hControl = @detect_section_content_control_by_id_class(sIdClass,hItem)
            break if !_.isUndefined hControl

    return hControl

  #+2013.11.27 tuiteraz
  get_data_object_route: (hNavItem)->
    "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}/#{hNavItem.sSlug}"

  #+2014.1.30 tuiteraz
  get_settings_object_route: (hNavItem)->
    "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.settings.sSlug}/#{hNavItem.sSlug}"

  #+2013.12.7 tuiteraz
  get_edit_item_route: (hNavItem,sItemId)->
    "/#{@hCfgCP.sSlug}/#{@hCfgCP.sections.data.sSlug}/#{hNavItem.sSlug}/#{sItemId}/edit"

  #+2013.12.2 tuiteraz
  is_tab_body_list_content_empty: (sRoute) ->
    jBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list tbody")

    if (jBody.length == 0) || _.isUndefined(jBody.length)
      # for settings section where we'r not using table for items
      jBody = $("[data-route='#{sRoute}'] .cp-content-tab-body-list form")

    if jBody.children().length == 0
      return true
    else
      return false