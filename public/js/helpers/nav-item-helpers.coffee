define [
  'beaver-console'
], (
  jconsole
) ->
  jconsole.info "nav-item-helpers"

  #+2014.3.1 tuiteraz
  init:(@ctrlParent)->
    @hCfgSite = @ctrlParent.hCfgSite

  #  из нав итема получить значения свойства по его имени из списка свойств
  # +2013.8.27 tuiteraz
  extra_prop_value_of: (hNavItem, sExtraPropName) ->
    Res = null
    if defined hNavItem.aExtraProperties
      if _.isObject hNavItem.aExtraProperties
        Res = hNavItem.aExtraProperties[sExtraPropName] if !_.isUndefined hNavItem.aExtraProperties[sExtraPropName]

    return Res

  # +2013.8.27 tuiteraz
  size_class_of: (hNavItem) ->
    iSize = @extra_prop_value_of(hNavItem,"iTitleTextSize")
    sRes = ""
    sRes = "size#{iSize}" if !_.isNull iSize

    return sRes

  # если не указан фильт тэгов, категорий и айди страницы то true
  # +2013.8.27 tuiteraz
  is_empty: (hNavItem) ->
    bNoPage = if _.isUndefined(hNavItem.id) or _.isEmpty(hNavItem.id) then true else false
    bNoTags = if _.isUndefined(hNavItem.aPostTags) or _.size(hNavItem.aPostTags)==0 then true else false
    bNoCats = if _.isUndefined(hNavItem.aPostCategories) or _.size(hNavItem.aPostCategories)==0 then true else false
    if !_.isUndefined(hNavItem.bEmpty)
      bRes = hNavItem.bEmpty
    else
      bRes = if (bNoPage and bNoTags and bNoCats) then true else false

    return bRes

  # +2013.8.27 tuiteraz
  is_group: (hNavItem) ->
    bRes = false

    if !_.isUndefined hNavItem.aChildrens
      bRes = true if _.size(hNavItem.aChildrens)>0

    return bRes

  #+2014.2.25 tuiteraz
  is_page: (hNI)->
    hNI.optNavType == "page"

  #+2014.2.25 tuiteraz
  is_showroom: (hNI)->
    hNI.optNavType == "showroom"

  #+2014.2.25 tuiteraz
  is_catalog: (hNI)->
    hNI.optNavType == "catalog"

  # +2013.8.29 tuiteraz
  detect_by_slug: (aNavItems,sNavSlug) ->
    hNavItem = undefined
    for hItem in aNavItems
      if hItem.sSlug == sNavSlug
        hNavItem = hItem
        break
      else
        if _.isArray hItem.aChildrens
          for hSubItem in hItem.aChildrens
            if hSubItem.sSlug == sNavSlug
              hNavItem = hSubItem
              break
          break if !_.isUndefined hNavItem

    return hNavItem

  # +2013.10.9 tuiteraz
  detect_by_attr: (aNavItems,hAttr) ->
    hNavItem = _.findWhere aNavItems,hAttr

    if !hNavItem
      # error or we are looking in children by _id
      hNavItem = undefined
      for hItem in aNavItems
        if _.isArray hItem.aChildrens
          hNavItem = _.findWhere hItem.aChildrens,hAttr
          break if !_.isUndefined hNavItem

    return hNavItem

  # +2013.8.29 tuiteraz
  detect_by_href: (aNavItems,sHref) ->
    sLogHeader = "detect_by_href() : "
    hNavItem = undefined

    #jconsole.log "#{sLogHeader} sHref=#{sHref}"
    aSlugs = @extract_slug_from_hash sHref
    #jconsole.dir aSlug
    hNavItem = @detect_by_slug aNavItems, _.last aSlugs

    return hNavItem

  #+2013.9.30 tuiteraz
  extract_slug_from_hash: (sHash) ->
    sDecodedHash = decodeURIComponent sHash

    aChunks = sDecodedHash.split '/'
    iLastIdx = _.size(aChunks)-1
    aChunks[iLastIdx] = _(aChunks[iLastIdx]).strLeft @hCfgSite.sUrlHashToken
    aRes = []
    for sChunk in aChunks
      if !_.isEmpty(sChunk)
        if !_(sChunk).include '#'
          aRes.push sChunk

    return aRes

  #+2013.10.16 tuiteraz
  get_href_for: (hNavItem,sParams=null) ->
    if sParams
      sParamsRef = sParams
    else
      sParamsRef = ""


    if hNavItem.sSlug
      sItemToken = hNavItem.sSlug
    else if hNavItem._id
      sItemToken = hNavItem._id
    else if hNavItem.id
      sItemToken = hNavItem.id
    else
      sItemToken = hNavItem.i18nTitle

    if defined SITE.sCurrLocaleSlug
      sLocale = SITE.sCurrLocaleSlug
    else
      sHash = History.getState().hash
      aHash = sHash.split "/"

      if aHash[1].length == 2
        sLocale = aHash[1]
      else
        sLocale = 'unkown'

    sRes = "/#{sLocale}/#{sItemToken}#{@hCfgSite.sUrlHashToken}#{sParamsRef}"

    return sRes





