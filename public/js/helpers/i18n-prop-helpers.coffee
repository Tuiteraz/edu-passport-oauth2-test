define [
  'beaver-console'
], (
  jconsole
) ->
  jconsole.info "i18n-prop-helpers"

  # will flatten alli18n* attr for current locale
  #+2014.1.3 tuiteraz
  flatten: (hItem,sLocaleSlug)->
    if _.isArray hItem
      aRes = []
      aRes.push @flatten(hItem1,sLocaleSlug) for hItem1 in hItem
      return aRes
    else
      _.each hItem, (Value,sPropName) =>
        if /^i18n[a-z\d_]+$/i.test sPropName
          hItem[sPropName] = hItem[sPropName][sLocaleSlug]
        else if _.isArray hItem[sPropName]
          _.each hItem[sPropName], (hArrayItem,iIdx)->
            _.each hArrayItem, (Value,sPropName2) =>
              if /^i18n[a-z\d_]+$/i.test sPropName2
                hItem[sPropName][iIdx][sPropName2] = hItem[sPropName][iIdx][sPropName2][sLocaleSlug]
        else if _.isObject(hItem[sPropName])
          hItem[sPropName] = @flatten hItem[sPropName], sLocaleSlug

      return hItem

  # get i18n prop names array from hItem or from prepared to send hData hash
  #+2014.1.3 tuiteraz
  get_list:(hData) ->
    aPropNames = _.keys hData
    aRes = []
    _.each aPropNames,(sPropName)=>
      if /^i18n[a-z\d_]+$/i.test sPropName
        aRes.push sPropName
      else
        if _.isObject hData[sPropName]
          aPropRes = @get_list hData[sPropName]
          aRes = _.extend aRes,aPropRes

    return aRes


