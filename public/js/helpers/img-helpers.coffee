define [
  "libs/beaver/templates"
  'beaver-console'
], (tmpl,jconsole) ->
  jconsole.info "img-helpers"

  jail_for : (sSelector) ->
    jconsole.debug "jail_for(#{sSelector})"
    $("#{sSelector} img.lazy[data-src]").jail {
      triggerElement: sSelector
      event: "scroll"

      effect: "fadeIn"
    }

  jail_hidden_for : (sSelector,bExtendSelector=true) ->
    jconsole.debug "jail_hidden_for(#{sSelector})"
    if bExtendSelector
      $("#{sSelector} img.lazy").jail {
        triggerElement: sSelector
        event: "scroll"
        loadHiddenImages:true
      }
    else
      $("#{sSelector}").jail {
        triggerElement: sSelector
        event: "scroll"

        loadHiddenImages:true
      }

  jail_hidden_with_placeholder_for : (sSelector) ->
    #jconsole.debug "jail_hidden_with_placeholder_for(#{sSelector})"
    $("#{sSelector} img.lazy").jail {
      triggerElement: sSelector
      event: "scroll"

      loadHiddenImages:true
      placeholder : @hCfgSite.sImgAJAXLoaderPath
    }



