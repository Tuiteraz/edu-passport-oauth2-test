define [
  "libs/beaver/templates"
  'beaver-console'
  'async'
  'helpers/cp-helpers'
  'helpers/model-helpers'
  'helpers/helpers'
], (
  tmpl
  jconsole
  async
  cp_hlprs
  model_hlprs
  hlprs
) ->
  jconsole.info "img-file-helpers"

  #+2014.2.5 tuiteraz
  before_filter: ->
    @sDialogId ||= "img-file-helpers-dialog"
    @bind_html_events_once = _.once(@bind_html_events) if _.isUndefined(@bind_html_events_once)

  #+2014.2.5 tuiteraz
  bind_html_events:() ->
    # MODAL
    $("##{@sDialogId}").modal {
      backdrop: true
      show: false
    }

    # after hiding need to correct window.location - remove item id
    $("##{@sDialogId}").on "hidden.bs.modal", (e)=>
      @remove_html()

  #+2014.2.5 tuiteraz
  is_html_exist: ->
    $("##{@sDialogId}").length > 0 ? true : false

  #+2014.2.19 tuiteraz
  render: (hImgFile) ->
    $("##{@sDialogId}").remove() if @is_html_exist()

    sModalParams = "tabindex='-1' role='dialog' aria-hidden='true'"
    sHtml = tmpl.div "modal fade", @sDialogId,sModalParams, [
      tmpl.div "modal-dialog modal-lg","","",[
        tmpl.div "modal-content","","",[
          tmpl.div "modal-header", [
            tmpl.button "close pull-right", "", "data-dismiss='modal' aria-hidden='true'", "&times;"
            tmpl.h 3,"Image","no-margin"
            tmpl.clearfix()
          ]

          tmpl.div "modal-body no-padding", [
            tmpl.div "row no-margin",[
              tmpl.div "col-md-9 img-container",[
                tmpl.img "/db/file/#{hImgFile._id}"
              ]
              tmpl.div "col-md-3 form-horizontal",[
                tmpl.div "form-group",[
                  tmpl.label "control-label col-md-4",["Name"]
                  tmpl.div "col-md-8 no-padding", [
                    tmpl.span "form-control",[hImgFile.sTitle]
                  ]
                ]
                tmpl.div "form-group",[
                  tmpl.label "control-label col-md-4",["Size"]
                  tmpl.div "col-md-8 no-padding", [
                    tmpl.span "form-control",[hImgFile.iSize]
                  ]
                ]

              ]
            ]
          ]
        ]
      ]
    ]
    $("body").append sHtml

  #+2014.2.5 tuiteraz
  remove_html: ->
    $("##{@sDialogId}").remove()

  #+2014.2.19 tuiteraz
  view:(ImgFile)->
    @before_filter()

    hImgFile = {}
    async.series [
      (fnNext)=>
        if _.isObject(ImgFile)
          hImgFile = ImgFile
          fnNext()
        else if typeof ImgFile == "string"
          # ImgFile is string file id - so get record
          mRefFile = model_hlprs.get_for_collection_name "ref_file"
          mRefFile.get {_id:ImgFile},{}, (hRes)=>
            if hRes.iStatus == 200
              hImgFile = hRes.aData[0]
              fnNext()
            else
              hlprs.show_results hRes
        else
          throw {
            name: 'Error'
            message: 'Expected an file object or string id of it '
          }

      (fnNext)=>
        @render hImgFile
        fnNext()

    ], =>
      $("##{@sDialogId}").modal "show"





