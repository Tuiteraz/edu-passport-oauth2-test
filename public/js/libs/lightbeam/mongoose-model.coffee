# requires jquery-1.9.1

define [
  'beaver-console'
  'helpers/i18n-prop-helpers'
], (
  jconsole
  i18n_prop_hlprs
) ->
  jconsole.info "mongoose-model"

  iInstanceCount = 0

  get_new_instance_id = ->
    ++iInstanceCount

  # hParams = { sCollectionName }
  #+2013.11.9 tuiteraz
  mModel = (@hParams)->
    @iInstanceID = get_new_instance_id()
    return this

  mModel.get_instance_count = ->
    return iInstanceCount

  # то что будет передаваться по наследству
  mModel.prototype = {
    #-- sys fn --

    get_instance_id : ->
      return @iInstanceID

    get_collection_name : ->
      return @hParams.sCollectionName

    #-- core fn --

    #+2013.11.6 tuiteraz
    create: (hData, fnCallback=null)->
      sUrl = "/db/#{@hParams.sCollectionName}/create"
      $.ajax {
        type: 'POST'
        url: sUrl
        dataType: 'JSON'
        data: hData
        async: true
        error:  (data,textStatus,jqXHR) ->
          data.responseJSON = j.parse_JSON(data.responseText)
          hRes =
            iStatus  : data.status
            sMessage : data.responseJSON.sMessage
          fnCallback(hRes) if _.isFunction fnCallback
        success:  (data,textStatus,jqXHR) ->
          hRes =
            iStatus : jqXHR.status
            aData   : data
          fnCallback(hRes) if _.isFunction fnCallback
      }

    # (id,{},fnCab)
    # (id,fnCab)
    # ({query},{params},fnCab)
    # ({query},fnCab)
    # чтобы получиь всю выборку передаем пустой запрос {} первым параметром
    #+2013.11.7 tuiteraz
    get: (Query, hOptions=null,fnCallback=null)->
      sUrl = "/db/#{@hParams.sCollectionName}/"

      Query = {_id:Query} if typeof(Query) == "string"
      sUrl += "?hQuery=#{JSON.stringify(Query)}&"

      if _.isFunction(hOptions)
        fnCallback = hOptions
        hOptions   = {}
      else
        hOptions ||={}

      sUrl += "hOptions=#{JSON.stringify(hOptions)}"

      if hOptions.bToZip
        $.fileDownload sUrl, {
          failCallback: (sResponseHtml,sUrl)->
            hRes =
              iStatus : 500
              sMessage : sResponseHtml
            fnCallback(hRes) if _.isFunction fnCallback

          successCallback: (sUrl)->
            hRes =
              iStatus : 200
              sMessage : "ok"
            fnCallback(hRes) if _.isFunction fnCallback
        }
      else
        $.ajax {
          type: 'GET'
          url: sUrl
          dataType: 'JSON'
          async: true
          error:  (data,textStatus,jqXHR) ->
            hRes =
              iStatus : jqXHR.status
              sMessage  : data.sMessage
            fnCallback(hRes) if _.isFunction fnCallback
          success:  (data,textStatus,jqXHR) ->
            hRes =
              iStatus : jqXHR.status
            if _.isArray data
              hRes.aData = data
            else
              hRes.aData = [data]

            fnCallback(hRes) if _.isFunction fnCallback
        }

    #+2013.10.31 tuiteraz
    new: (fnCallback=null)->
      sUrl = "/db/#{@hParams.sCollectionName}/new"
      $.ajax {
        type: 'GET'
        url: sUrl
        dataType: 'JSON'
        async: true
        error:  (data,textStatus,jqXHR) ->
          hRes =
            iStatus : jqXHR.status
            sMessage : data.sMessage
          fnCallback(hRes) if _.isFunction fnCallback
        success:  (data,textStatus,jqXHR) ->
          hRes =
            iStatus : jqXHR.status
            aData   : data.responseJSON
          fnCallback(hRes) if _.isFunction fnCallback
      }


    #+2013.11.8 tuiteraz
    remove: (Query,hOptions={},fnCallback=null)->
      sUrl = "/db/#{@hParams.sCollectionName}/"

      Query = {_id:Query} if typeof(Query) == "string"
      sUrl += "?hQuery=#{JSON.stringify(Query)}&"

      if _.isFunction(hOptions)
        fnCallback = hOptions
        hOptions   = {}
      else
        hOptions ||={}

      sUrl += "hOptions=#{JSON.stringify(hOptions)}"

      $.ajax {
        type: 'DELETE'
        url: sUrl
        dataType: 'JSON'
        async: true
        error:  (data,textStatus,jqXHR) ->
          hRes =
            iStatus : jqXHR.status
            sMessage  : data.sMessage

          fnCallback(hRes) if _.isFunction fnCallback
        success: (data,textStatus,jqXHR) ->
          hRes =
            iStatus : jqXHR.status
            sMessage :data.sMessage

          fnCallback(hRes) if _.isFunction fnCallback
      }

    # ({query}{data},{params},fnCab)
    #+2013.11.9 tuiteraz
    update: (hQuery, hData, hOptions={},fnCallback=null)->
      sUrl = "/db/#{@hParams.sCollectionName}/"


      if _.isFunction(hOptions) && !fnCallback
        fnCallback = hOptions
        hOptions = {}

      aI18nProps = i18n_prop_hlprs.get_list hData
      hI18nOptions =
        aI18nProperties : aI18nProps

      hOptions = _.extend hOptions, hI18nOptions

      hRequestData =
        hQuery: JSON.stringify(hQuery)
        hData: JSON.stringify(hData)
        hOptions: JSON.stringify(hOptions)

      $.ajax {
        type: 'PUT'
        url: sUrl
        dataType: 'JSON'
        async: true
        data : hRequestData
        error:  (data,textStatus,jqXHR) ->
          data.responseJSON = j.parse_JSON(data.responseText)
          hRes =
            iStatus  : data.status
            sMessage : data.responseJSON.sMessage
          fnCallback(hRes) if _.isFunction fnCallback
        success:  (data,textStatus,jqXHR) ->
          hRes =
            iStatus : jqXHR.status
            iNumberAffected : data.iNumberAffected

          if !_.isUndefined(data.hArrayData)
            hRes["ArrayData"] = data.hArrayData

          fnCallback(hRes) if _.isFunction fnCallback
      }

  }

  return mModel