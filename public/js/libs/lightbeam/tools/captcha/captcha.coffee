define [
  'libs/beaver/templates'
  'beaver-console'
], (tmpl,jconsole) ->
  jconsole.info "lightbeam/tools/captcha"

  # +2013.6.1 tuiteraz
  bind_events: ->
    $("#captcha img").unbind('click').click ->
      $(this).toggleClass 'active'
      if $(this).hasClass 'active'
        sThisSrc= $(this).attr 'src'
        $("#captcha img[src!='#{sThisSrc}'].active").removeClass 'active'

  # +2013.6.2 tuiteraz
  selected_number: ->
    if @is_selected()
      sImgSrc = $("#captcha .active").attr 'src'
      hImg = _.detect @aImages, (hImg)->
        sImgSrc.search(hImg.sSrc) >= 0
      iIdx = _.indexOf @aImages,hImg
      return iIdx + 1
    else
      return 0

  set_control_number: (iNewNumber) ->
    @iControlNumber = iNewNumber

  # +2013.6.1 tuiteraz
  # *2013.9.4 tuiteraz: +sJuicyUrl
  get_control_number: ()->
    me = this

    $.ajax {
      type: 'GET'
      url: "/captcha"
      dataType: 'text'
      async: false
      error:  (data,textStatus,jqXHR) ->
        jconsole.error data

      success:  (data,textStatus,jqXHR) ->
        me.iControlNumber = parseInt data
        me.update_title()
    }

    return @iControlNumber

  # +2013.6.2 tuiteraz
  is_selected: ->
    if $("#captcha .active").length == 1 then true else false

  # +2013.4.26 tuiteraz
  render: () ->

    @get_control_number()

    hInput =
      id: "captcha"
      sFullName: 'Captcha'
      sInputStyle: FRM.input.style.input
      sInputType: FRM.input.type.hidden

    sLocaleSlug = @update_curr_locale()

    aTitles= [
      { "en":"house", "de":"house", "ru":"дом" }
      { "en":"key",   "de":"key",   "ru":"ключ" }
      { "en":"flag",  "de":"flag",  "ru":"флаг" }
      { "en":"watch", "de":"watch", "ru":"часы" }
      { "en":"bug",   "de":"bug",   "ru":"жука" }
      { "en":"pen",   "de":"pen",   "ru":"ручку" }
      { "en":"light", "de":"light", "ru":"лампочку" }
      { "en":"note",  "de":"note",  "ru":"ноту" }
      { "en":"heart", "de":"heart", "ru":"сердце" }
      { "en":"globe", "de":"globe", "ru":"глобус" }
    ]

    sDir = "/js/libs/lightbeam/tools/captcha/images/"
    @aImages = [
      {sTitle: aTitles[0][sLocaleSlug], sSrc:"#{sDir}01.png"}
      {sTitle: aTitles[1][sLocaleSlug], sSrc:"#{sDir}02.png"}
      {sTitle: aTitles[2][sLocaleSlug], sSrc:"#{sDir}03.png"}
      {sTitle: aTitles[3][sLocaleSlug], sSrc:"#{sDir}04.png"}
      {sTitle: aTitles[4][sLocaleSlug], sSrc:"#{sDir}05.png"}
      {sTitle: aTitles[5][sLocaleSlug], sSrc:"#{sDir}06.png"}
      {sTitle: aTitles[6][sLocaleSlug], sSrc:"#{sDir}07.png"}
      {sTitle: aTitles[7][sLocaleSlug], sSrc:"#{sDir}08.png"}
      {sTitle: aTitles[8][sLocaleSlug], sSrc:"#{sDir}09.png"}
      {sTitle: aTitles[9][sLocaleSlug], sSrc:"#{sDir}10.png"}
    ]

    @aImages = _.shuffle @aImages



    sImgHtml = ''
    for hImg in @aImages
      sImgHtml += tmpl.img hImg.sSrc

    tmpl.div "","captcha", "align='center' ", [
      tmpl.twbp_input hInput
      tmpl.div "title", "", [ ]
      tmpl.div "", "captcha-image-container","align='center'", [ sImgHtml ]
    ]

  update_curr_locale:()->
    if defined SITE.sCurrLocaleSlug
      sLocaleSlug = SITE.sCurrLocaleSlug
    else
      sHash = History.getState().hash
      aHash = sHash.split "/"

      if aHash[1].length == 2
        sLocaleSlug = aHash[1]
        SITE.sCurrLocaleSlug = sLocaleSlug

    return sLocaleSlug

  # после асинхронного получения контрольного номера обновим заголвоок
  # +2013.6.3 tuiteraz
  # *2013.9.4 tuiteraz
  update_title: ->
    if defined @aImages
      sLocaleSlug = @update_curr_locale()
      sTitle = "In order to confirm that you are human, select" if sLocaleSlug == 'en'
      sTitle = "In order to confirm that you are human, select" if sLocaleSlug == 'de'
      sTitle = "Чтобы подтвердить, что Вы человек, выберите" if sLocaleSlug == 'ru'

      sImgTitle = @aImages[@iControlNumber-1].sTitle
      sTitleHtml = "#{sTitle} <span style='font-weight: bold;' >'#{sImgTitle}'</span>"
      $("#captcha .title").slideUp().empty().append(sTitleHtml).slideDown()




