define [
  'beaver-console'
  './captcha/captcha'
  'helpers/helpers'
  'lightbeam-settings'
], (jconsole
    captcha
    hlprs
) ->
  jconsole.info "lightbeam/tools/email"

  #+2013.11.13 tuiteraz
  send: (sFrom,sTo,sSubject,sHtml,fnCallback=null)->
    hEmailToMe=
      sFrom    : sFrom
      sTo      : sTo
      sSubject : sSubject
      sHtml    : sHtml

    hToServerData =
      Email: [hEmailToMe]
      iCaptchaControlNumber: captcha.get_control_number()


    # отправить аяксом на сервер для отправки
    me = this

    $.ajax {
      type: 'POST'
      url: "email"
      dataType: 'JSON'
      async: true
      data: hToServerData
      error:  (data,textStatus,jqXHR) ->
        hlprs.hide_modal_alert()
        response = j.parse_JSON data.responseText
        #me.show_ajax_alerts response.Alerts if defined response.Alerts
        if defined response.iCaptchaControlNumber
          captcha.set_control_number parseInt response.iCaptchaControlNumber
        fnCallback(response) in _.isFunction(fnCallback)
      success:  (data,textStatus,jqXHR) ->
        response = data

        if defined response.iCaptchaControlNumber
          captcha.set_control_number parseInt response.iCaptchaControlNumber

        fnCallback(response) in _.isFunction(fnCallback)
    }

