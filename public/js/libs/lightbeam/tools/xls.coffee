define [
  'beaver-console'
  'helpers/helpers'
  'libs/beaver/templates'
  'async'
  'lightbeam-settings'
], (jconsole
    hlprs
    tmpl
    async
) ->
  jconsole.info "tools/xls"

  #+2013.11.14 tuiteraz
  import_choosen_file: (jThis,fnCallback=null)->
    aFiles = []
    async.series [
      (fnNext)->
        $.each jThis.files, (iIdx, jFile)->
          jReader = new FileReader()
          jReader.onload = (e)->
            aFiles.push {
              sFilename: jFile.name
              sData: e.target.result
            }
            fnNext null,aFiles

          jReader.readAsDataURL(jFile)
      (fnNext)=>
        hData = {
          sFileName: aFiles[0].sFilename
          sFileData: aFiles[0].sData
        }

        sData = JSON.stringify(hData)
        $.ajax {
          type: 'POST'
          url: "/xls"
          async: true
          dataType: "JSON"
          data: hData
          beforeSend: ()->
            hlprs.progress.show()
          progressUpload: (e,iPercentComplete) ->
            hlprs.progress.set_current_value(iPercentComplete)

          error:  (data,textStatus,jqXHR) ->
            jconsole.debug "XLS.import_choosen_file():AJAX-ERROR "
            hRes =
              iStatus : data.iStatus
              sMessage : data.sMessage

            fnNext(null,hRes)

          success:  (data,textStatus,jqXHR) ->
            jconsole.debug "XLS.import_choosen_file():AJAX-SUCCESS "
            hlprs.progress.hide()
            jqXHR.responseJSON = j.parse_JSON(jqXHR.responseText) if !jqXHR.responseJSON

            hRes =
              iStatus : jqXHR.status
              aData   : data.aData

            fnNext null,hRes
        }

    ],(sErr,aRes) ->
      jconsole.error sErr if sErr
      fnCallback(aRes[1]) if _.isFunction(fnCallback)

  #+2013.11.14 tuiteraz
  choose_file: (fnCallback) ->
    #append hidden form&input
    if $("#lightbeam-xls-upload-file").length == 0
      $('body').append tmpl.form "hidden","lightbeam-xls-upload-file","action='/xls' method='post' enctype='multipart/form-data'", [
        tmpl.input "hidden","lightbeam-xls-file","type='file' name='FileToUpload'",[]
      ]
    $("#lightbeam-xls-file").trigger 'click'
    $("#lightbeam-xls-file").unbind('change').change (e)->
      fnCallback(e.target)

  is_choosen: ->
    if $("#lightbeam-xls-file").length == 1
      if !_.isEmpty($("#lightbeam-xls-file").val())
        return true
      else
        return false
    else
      return false


