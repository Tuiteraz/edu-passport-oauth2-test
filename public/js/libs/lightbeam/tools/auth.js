// Generated by CoffeeScript 1.6.3
(function() {
  define(['beaver-console', 'lightbeam-settings'], function(jconsole) {
    jconsole.info("lightbeam/tools/auth");
    return {
      get_auth_status: function(fnCallback) {
        var bAsync, hRes;
        if (fnCallback == null) {
          fnCallback = null;
        }
        if (_.isFunction(fnCallback)) {
          bAsync = true;
        } else {
          bAsync = false;
          hRes = {};
        }
        $.ajax({
          type: 'GET',
          url: "/" + LIGHTBEAM.auth.sToolPath,
          dataType: 'json',
          async: bAsync,
          error: function(data, textStatus, jqXHR) {
            hRes = {
              iStatus: data.iStatus,
              sMessage: data.sMessage
            };
            if (_.isFunction(fnCallback)) {
              return fnCallback(hRes);
            }
          },
          success: function(data, textStatus, jqXHR) {
            hRes = {
              iStatus: jqXHR.status,
              bIsSignedIn: data.bIsSignedIn,
              hUser: data.hUser,
              sMessage: data.sMessage
            };
            if (_.isFunction(fnCallback)) {
              return fnCallback(hRes);
            }
          }
        });
        if (!bAsync) {
          return hRes;
        }
      },
      sign_in: function(hData, fnCallback) {
        var bAsync, hRes;
        if (fnCallback == null) {
          fnCallback = null;
        }
        if (_.isFunction(fnCallback)) {
          bAsync = true;
        } else {
          bAsync = false;
          hRes = {};
        }
        $.ajax({
          type: 'POST',
          url: "/" + LIGHTBEAM.auth.sToolPath,
          dataType: 'json',
          data: hData,
          async: bAsync,
          error: function(data, textStatus, jqXHR) {
            var hResponseJSON;
            hResponseJSON = j.parse_JSON(data.responseText);
            hRes = {
              iStatus: data.status,
              bIsSignedIn: hResponseJSON.bIsSignedIn,
              hUser: hResponseJSON.hUser,
              sMessage: hResponseJSON.sMessage
            };
            if (_.isFunction(fnCallback)) {
              return fnCallback(hRes);
            }
          },
          success: function(data, textStatus, jqXHR) {
            hRes = {
              iStatus: jqXHR.status,
              bIsSignedIn: data.bIsSignedIn,
              hUser: data.hUser,
              sMessage: data.sMessage
            };
            if (_.isFunction(fnCallback)) {
              return fnCallback(hRes);
            }
          }
        });
        if (!bAsync) {
          return hRes;
        }
      },
      sign_out: function(fnCallback) {
        var bAsync, hRes;
        if (fnCallback == null) {
          fnCallback = null;
        }
        if (_.isFunction(fnCallback)) {
          bAsync = true;
        } else {
          bAsync = false;
          hRes = {};
        }
        $.ajax({
          type: 'DELETE',
          url: "/" + LIGHTBEAM.auth.sToolPath,
          dataType: 'json',
          async: bAsync,
          error: function(data, textStatus, jqXHR) {
            var hResponseJSON;
            hResponseJSON = j.parse_JSON(data.responseText);
            hRes = {
              iStatus: data.status,
              bIsSignedIn: hResponseJSON.bIsSignedIn,
              sMessage: hResponseJSON.sMessage
            };
            if (_.isFunction(fnCallback)) {
              return fnCallback(hRes);
            }
          },
          success: function(data, textStatus, jqXHR) {
            hRes = {
              iStatus: jqXHR.status,
              bIsSignedIn: data.bIsSignedIn,
              sMessage: data.sMessage
            };
            if (_.isFunction(fnCallback)) {
              return fnCallback(hRes);
            }
          }
        });
        if (!bAsync) {
          return hRes;
        }
      },
      if_signed_in: function(fnCallback) {
        var _this = this;
        return this.get_auth_status(function(hAuthStatus) {
          if (hAuthStatus.bIsSignedIn && hAuthStatus.hUser.bIsAdmin) {
            return fnCallback() === _.isFunction(fnCallback);
          }
        });
      }
    };
  });

}).call(this);

/*
//@ sourceMappingURL=auth.map
*/
