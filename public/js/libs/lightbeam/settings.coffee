window.LIGHTBEAM =
  auth:
    sToolPath: "auth"
    form:
      controls:
        email:
          id: "cp-login-frm-email"
          sParams: "type='email' placeholder='admin@mail.com'"
        password:
          id: "cp-login-frm-password"
          sParams: "type='password'"
      buttons:
        login:
          id: "cp-login-btn-login"
          sClass: "btn btn-primary btn-block"
          sTitle: "Login"
          sParams: "type='submit'"
