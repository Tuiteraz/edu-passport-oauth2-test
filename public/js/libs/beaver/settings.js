// Generated by CoffeeScript 1.6.3
(function() {
  window.FRM = {
    input: {
      style: {
        input: 'input',
        image: 'image',
        text: 'textarea',
        radio_v: 'radio-vertical',
        radio_h: 'radio-horizontal',
        select: 'select',
        checkbox: 'checkbox',
        file: 'file'
      },
      type: {
        hidden: 'hidden',
        text: 'text',
        number: 'number',
        checkbox: 'checkbox',
        password: 'password'
      }
    },
    list: {
      action: {
        view: {
          id: 'view-item',
          icon: 'eye-open',
          tooltip: 'Открыть элемент'
        },
        destroy: {
          id: 'destroy-item',
          icon: 'trash',
          tooltip: 'Удалить элемент'
        }
      }
    }
  };

  window.LENGTH_TOOLTIP = {
    id: "juicy-input-length-tooltip",
    inactive_timeout: 5000,
    after_focus_timeout: 1000
  };

}).call(this);

/*
//@ sourceMappingURL=settings.map
*/
