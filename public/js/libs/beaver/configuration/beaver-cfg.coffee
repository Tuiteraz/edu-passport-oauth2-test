define [],() ->
  #+2013.12.17 tuiteraz
  get_content: ()->
    {
      dialog:
        id : "beaver-helper-dialog"
        buttons:
          ok:
            sClass: "btn-default"
            sIcon: "thumbs-up"
            sAction: "beaver-dialog-ok"
            sActionAttrName : "data-action"
            sInnerText : "Ok"
            sTag : "button"
          yes:
            sClass: "btn-default"
            sIcon: "thumbs-up"
            sAction: "beaver-dialog-yes"
            sActionAttrName : "data-action"
            sInnerText : "Yes"
            sTag : "button"
          no:
            sClass: "btn-default"
            sIcon: "thumbs-down"
            sAction: "beaver-dialog-no"
            sActionAttrName : "data-action"
            sInnerText : "No"
            sTag : "button"
          cancel:
            sClass: "btn-default btn-warning"
            sIcon: "ban"
            sAction: "beaver-dialog-cancel"
            sActionAttrName : "data-action"
            sInnerText : "Cancel"
            sTag : "button"

    }