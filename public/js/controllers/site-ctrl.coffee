define [
  'beaver-console'
  'crossroads'
  'libs/beaver/templates'
  'helpers/helpers'
  'helpers/img-helpers'
  'helpers/nav-item-helpers'
  'templates/site-tmpl'
  'lightbeam-helpers'
  "async"
  'configuration/site-cfg'
], (
  jconsole,
  crossroads,
  tmpl,hlprs,img_hlprs,nav_item_hlprs,
  site_tmpl,
  lb_hlprs
  async
  cfgSite
) ->
  jconsole.info "site-ctrl"

  #+2013.10.30 tuiteraz
  init: () ->
    @sLogHeader = "site-ctrl"

    hlprs.load_stylesheet "/css/site.css", =>
      jconsole.log "-> site.css loaded"
      jconsole.group "#{@sLogHeader}.init() [async-series]"

      async.series [
        (fnNext)=>
          @get_configuration fnNext

        (fnNext)=>
          @init_templates()
          fnNext()

        (fnNext)=>
          @render()
          @bind_events()
          fnNext()

      ], ()->
        jconsole.info "finished"
        jconsole.group_end()


  #+2014.1.27t uiteraz
  init_templates:()->
    jconsole.log "#{@sLogHeader}.init_templates()"
    site_tmpl.init @hCfgSite

  #+2013.10.30 tuiteraz
  bind_events:(bMdlOnly=false) ->
    me = this
    jconsole.log "#{@sLogHeader}.bind_events(bMdlOnly=#{bMdlOnly})"

    History.Adapter.bind window, "statechange", =>
      hState = History.getState()
      sHash = hState.hash
      sHash += "#"+hState.data.sLastOpenedItemId if !_.isUndefined(hState.data.sLastOpenedItemId)
      @Router.parse sHash

    $(me).unbind('click').click (e,hDetails) ->
      if defined hDetails.sAction
        hlprs.goto(hDetails)              if hDetails.sAction == @hCfgSite.actions.goto_nav_link
        @event_on_br_check_in(e,hDetails) if hDetails.sAction == @hCfgSite.actions.check_in
        @event_on_br_sign_in(e,hDetails)  if hDetails.sAction == @hCfgSite.actions.sign_in
        @event_on_br_submit_request(e,hDetails) if hDetails.sAction == @hCfgSite.actions.submit_request


    @bind_html_events()

  #+2013.11.26 tuiteraz
  bind_html_events: ()->
    me = this


    # cp content tab header & items btns
    sSelector = "button[data-content-action]"
    $(document).delegate sSelector, "click", (e)->
      sAction = $(this).attr "data-content-action"
      send_event_to me, {sAction:sAction, jThis:$(this)}

    $(window).resize =>
      clearTimeout(@iResizeTimerId) if @iResizeTimerId
      @iResizeTimerId = setTimeout =>
        @event_on_window_resize()
      , 100

    @event_on_window_resize()

  event_on_br_check_in: (e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_br_check_in()"

    sReqUrl = "#{@hCfgSite.bleacher_report.sApiPath}#{@hCfgSite.bleacher_report.sUserPath}"

    hData =
      token : @hUser.token

    async.series [
      (fnNext)=>
        $.ajax {
          type: 'POST'
          url: sReqUrl
          dataType: 'json'
          data: hData
          async: true
          error:  (data,textStatus,jqXHR) ->
            hResponseJSON = j.parse_JSON(data.responseText)
            fnNext(hResponseJSON.sMessage)

          success:  (data,textStatus,jqXHR) ->
            hRes =
              iStatus     : jqXHR.status
              hUser       : data

            fnNext(null,hRes)
        }

    ], (sErr,aRes)=>
      if !sErr
        @hUser = aRes[0].hUser
        @update_check_in_res_html()

  event_on_br_sign_in: (e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_br_sign_in()"

    hEmail  = @hCfgSite.content.bleacher_report.controls.sEmail
    hPassword  = @hCfgSite.content.bleacher_report.controls.sPassword

    sEmail = j.frm.control.get_data(hEmail)
    sPwd   = j.frm.control.get_data(hPassword)

    jconsole.log "sEmail:#{sEmail.Value}"
    jconsole.log "sPwd:#{sPwd.Value}"

    sReqUrl = "#{@hCfgSite.bleacher_report.sApiPath}#{@hCfgSite.bleacher_report.sLoginPath}"

    hData =
      user :
        email    : sEmail.Value
        password : sPwd.Value

    async.series [
      (fnNext)=>
        $.ajax {
          type: 'POST'
          url: sReqUrl
          dataType: 'json'
          data: hData
          async: true
          error:  (data,textStatus,jqXHR) ->
            hResponseJSON = j.parse_JSON(data.responseText)
            fnNext(hResponseJSON.sMessage)

          success:  (data,textStatus,jqXHR) ->
            hRes =
              iStatus     : jqXHR.status
              hUser       : data

            fnNext(null,hRes)
        }

    ], (sErr,aRes)=>
      if !sErr
        @hUser = aRes[0].hUser
        @update_auth_token_html()
        sSelector = "[data-content-action='#{@hCfgSite.content.bleacher_report.buttons.check_in.sAction}']"
        $(sSelector).removeClass "disabled"
        sSelector = "[data-content-action='#{@hCfgSite.content.bleacher_report.buttons.submit.sAction}']"
        $(sSelector).removeClass "disabled"


  event_on_br_submit_request: (e,hDetails)->
    jconsole.log "#{@sLogHeader}.event_on_br_submit_request()"

    hDate      = @hCfgSite.content.bleacher_report.controls.sDate
    sDate      = j.frm.control.get_data(hDate).Value
    sTimestamp = parseInt(new Date(sDate).getTime()/1000)

    sReqUrl = "#{@hCfgSite.bleacher_report.sApiPath}#{@hCfgSite.bleacher_report.sPushNotificationsPath}"

    aTags = _.map @hUser.api.teams, (hTeam)->
      hTeam.shortName

    sTags = ""
    sTags += "#{sTag}," for sTag in aTags
    sTags = _.strLeftBack sTags,","

    hData =
      tags         : sTags
      last_request : sTimestamp

    async.series [
      (fnNext)=>
        $.ajax {
          type: 'GET'
          url: sReqUrl
          dataType: 'json'
          data: hData
          async: true
          error:  (data,textStatus,jqXHR) ->
            hResponseJSON = j.parse_JSON(data.responseText)
            fnNext(hResponseJSON.sMessage)

          success:  (data,textStatus,jqXHR) ->
            hRes =
              iStatus     : jqXHR.status
              hData       : data

            fnNext(null,hRes)
        }

    ], (sErr,aRes)=>
#      if !sErr
#        @hUser = aRes[0].hUser
#        @update_auth_token_html()
#        sSelector = "[data-content-action='#{@hCfgSite.content.bleacher_report.buttons.check_in.sAction}']"
#        $(sSelector).removeClass "disabled"

  #+2014.1.29 tuiteraz
  event_on_window_resize:()->
    jconsole.log "#{@sLogHeader}.event_on_window_resize().start"

#    iWndWidth = $(window).width()
#    iSectionSidebarWidth = iWndWidth * 0.16
#    iSectionSidebarCntWidth = iSectionSidebarWidth
#    iSectionContentWidth = iWndWidth * 0.84
#
#    $(".cp-section-sidebar").css {
#      width: iSectionSidebarWidth
#    }
#
     # sidebar nav content bug -> force width -3px to equalto cntr
#    $(".cp-section-sidebar > div").css {
#      width: (iSectionSidebarCntWidth-2)
#    }
#
#    $(".cp-section-content").css {
#      width: iSectionContentWidth
#    }

#    jconsole.log "#{@sLogHeader}.event_on_window_resize().finish"

  # +2014.1.27 tuiteraz
  get_configuration: (fnCallback)->
    jconsole.log "#{@sLogHeader}.get_configuration()"

    @hCfgSite = cfgSite.get_content()
    hlprs.call_if_function(fnCallback)


  #+2013.10.30 tuiteraz
  render: ->
    $('body').addClass "body-padding body-cp"
    $('body').empty().append site_tmpl.render()

  # after nav item click default reaction is disabled to prevent page reload
  # but we still need to update nav item class state
  #+2014.1.127 tuiteraz
  update_nav_items_state:(sHref)->
    jconsole.log "#{@sLogHeader}.update_nav_items_state(#{sHref})"
    #check section nav item routes or root section routes
    $(".nav:visible li.active a[href!='#{sHref}']").parent().removeClass "active"
    $(".nav:visible li a[href='#{sHref}']").parent().addClass "active"

    # in case of route /cp/data/text-snippets top-navbar ni-item would not be set to .active
    if _.count(sHref,"/") == 3
      sRoute = _.strLeftBack(sHref,'/')
      @update_nav_items_state(sRoute)

  #+2014.4.7 tuiteraz
  update_auth_token_html:()->
    jconsole.log "#{@sLogHeader}.update_auth_token_html()"
    me = this
    $("#site-secure-token").fadeOut 'slow',()->
      $(this).text me.hUser.token
      $(this).fadeIn('slow')

  #+2014.4.7 tuiteraz
  update_check_in_res_html:()->
    jconsole.log "#{@sLogHeader}.update_check_in_res_html()"
    me = this
    $("#site-check-in-res").fadeOut 'slow',()->
      $(this).text "User's name is: #{me.hUser.full_name}"
      $(this).fadeIn('slow')
