window.bJvmConsoleEnabled = true
window.sJvmConsoleLogContext = "APP"

if typeof requirejs == "undefined"
  jHead = document.getElementsByTagName('head')[0]
  jScript = document.createElement('script')
  jScript.setAttribute('type','text/javascript')
  jScript.setAttribute('src','/js/libs/require-js/require.js')
  jScript.setAttribute('data-main','/js/app.js')
  jHead.appendChild(jScript)

  return

console.groupCollapsed("loading modules & confs ...") if bJvmConsoleEnabled

requirejs.config {
  waitSeconds: 5
  paths: {
    'jquery'             : 'libs/jquery/jquery-2.0.3'
    'jquery-plugins'     : 'libs/jquery/jquery.plugins'

    'domReady'           : 'libs/require-js/plugins/domReady'
    'lib-pack'           : 'libs/lib.pack'
    'stylesheets-pack'   : '../css/stylesheets.pack'
    'require-js'         : 'libs/require-js/require'

    'beaver-console'     : 'libs/beaver/console'
    'beaver-extensions'  : 'libs/beaver/extensions'
    'beaver-settings'    : 'libs/beaver/settings'

    'lightbeam-helpers'  : 'libs/lightbeam/helpers'
    'lightbeam-settings' : 'libs/lightbeam/settings'

    'underscore'         : 'libs/underscore'
    'underscore.string'  : 'libs/underscore.string.min'
    'bootstrap'          : 'libs/bootstrap/js/bootstrap.min'
    'bootstrap-hover-dropdown' : 'libs/bootstrap/js/twitter-bootstrap-hover-dropdown'
    'crossroads'         : 'libs/crossroads'
    'async'              : 'libs/async'
    'moment'             : 'libs/moment.min'
    'hasher'             : 'libs/hasher/wrapper'
    'signals'            : 'libs/signals'

  }
  shim: {
    'jquery'                         : { exports: 'jQuery' }
    'jquery-plugins'                 : ['jquery']
    'libs/beaver/helpers'            : ['jquery']
    'bootstrap'                      : ['jquery']
    'bootstrap-hover-dropdown'       : ['bootstrap']


    'stubs/slides-stub'              : ['settings']

    'beaver-console'                 : ['beaver-extensions']
    'beaver-extensions'              : ['jquery','underscore']

    'lib-pack'                       : ['beaver-console']
    'stylesheets-pack'               : ['beaver-extensions']
    'underscore.string'              : ['underscore']

  }
}

requirejs [
  'lib-pack'
  'beaver-console'
  'domReady'
  'controllers/site-ctrl'
  'stylesheets-pack'
], (mdlLibPack,jconsole,domReady,ctrlSite) ->
  jconsole.group_end()
  jconsole.info "STARTING APP..."

  domReady ->
    jconsole.log "DOM ready now!!"
    ctrlSite.init()

    $("meta[name=fragment]").remove() # because search engine would't accept html snapshot with this tag


