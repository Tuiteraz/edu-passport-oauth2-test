define [
  'beaver-console'
  'moment'
],(
  jconsole
  moment
) ->
  jconsole.info "configuration/site-cfg"

  #+2014.3.1 tuiteraz
  get_content:()->
    @hContentButtonActions =
      submit_request     : "site-content-submit-request"
      sign_in            : "site-content-sign-in"
      check_in           : "site-content-check-in"

    {
      sUrlHashToken : "#"
      sImgAJAXLoaderPath : "/images/ajax-loaders/blank.gif"
      actions:
        goto_tab_link: "goto-tab-link" # local ctrl's routes like #album-tab-view
        goto_nav_link: "goto-nav-link"
        goto_post_link: "goto-post-link"
        goto_route: "goto-route"
        submit_request : @hContentButtonActions.submit_request
        sign_in        : @hContentButtonActions.sign_in
        check_in       : @hContentButtonActions.check_in
      sBrowserMainTitle: "BR-WRAPPER"
      iInnerContentWidth: 990 # from css
      nav:
        sTitle: "DataArt: Passport - OAuth2"
        container_id: "site-nav-container"
        iHeight: 50
      content:
        bleacher_report:
          buttons:
            submit:
              sClass: "btn-primary disabled"
              sIcon: "bolt"
              sAction: @hContentButtonActions.submit_request
              sActionAttrName : "data-content-action"
              sInnerText : "Submit"
              sTag : "button"
            sign_in:
              sClass: "btn-primary"
              sIcon: "user"
              sAction: @hContentButtonActions.sign_in
              sActionAttrName : "data-content-action"
              sInnerText : "Sign in"
              sTag : "button"
            check_in:
              sClass: "btn-primary disabled"
              sIcon: "user"
              sAction: @hContentButtonActions.check_in
              sActionAttrName : "data-content-action"
              sInnerText : "Check in by token"
              sTag : "button"

          controls:
            sEmail:
              id: "site-email"
              sParams: "type='email' placeholder='my-email@mail.com'"
              sFullName    : "Email"
              sInputStyle  : FRM.input.style.input
              sInputType   : FRM.input.type.text
              bRequired    : false
              sLabel       : "Email"
              sLabelClass  : "col-md-3"
              sSizeClass   : "col-md-8"

            sPassword:
              id: "site-password"
              sFullName    : "Password"
              sInputStyle  : FRM.input.style.input
              sInputType   : FRM.input.type.password
              bRequired    : false
              sLabel       : "Password"
              sLabelClass  : "col-md-3"
              sSizeClass   : "col-md-8"
      page:
        container_class: "page-container" # слайд старницы с фиксированным размером
        content_class: "page-content"
        iHeaderHeight: 90
      load_waiter_text: "<i class='fa fa-spinner fa-spin fa-3x'></i>"

    }

