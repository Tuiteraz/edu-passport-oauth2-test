define [
  'libs/beaver/templates'
  'helpers/helpers'
  'beaver-console'
  'moment'
], (tmpl,hlprs,jconsole) ->

  jconsole.info "cp-tmpl"

  #+2014.1.27 tuiteraz
  init: (@hCfgSite)->

  #+2014.1.27 tuiteraz
  render: () ->

    tmpl.div "",@hCfgSite.container_id, [
      @top_navbar()
      tmpl.div "cp-section-container container", "","",[
        tmpl.div "cp-section-content","",[
          tmpl.form "form-horizontal","","", [
            tmpl.div "row",[
              tmpl.div "col-md-12",[
                tmpl.h "3","Authenticate"
              ]
              tmpl.div "col-md-6 well",[
                tmpl.twbp_input @hCfgSite.content.bleacher_report.controls.sEmail, "alexey.gonchar@dataart.com"
                tmpl.twbp_input @hCfgSite.content.bleacher_report.controls.sPassword, "dataartenjoyit"
                tmpl.icon_button @hCfgSite.content.bleacher_report.buttons.sign_in
              ]
              tmpl.div "col-md-12","",[
                tmpl.h 3,"Secure token:"
                tmpl.h 3,"<>","","id='site-secure-token' style='word-wrap:break-word;'"
              ]
            ]
          ]
        ]
      ]
    ]

  #+2014.1.27 tuiteraz
  top_navbar: ->

    sNavItemsHtml = tmpl.ul "nav navbar-nav",[ "" ]

    tmpl.nav "navbar navbar-default navbar-fixed-top",@hCfgSite.nav.container_id,"role='navigation'", [
      tmpl.div "navbar-inner",[
        tmpl.div "navbar-header", "", "",[
          tmpl.a "/","",[@hCfgSite.nav.sTitle],"navbar-brand"
        ]
        tmpl.div "col-md-6", "", [
          sNavItemsHtml
        ]
        tmpl.clearfix()
      ]
    ]

  #+2014.1.29 tuiteraz
  top_navbar_section: (hSection) ->
    sHref = "/#{@hCfgSite.sSlug}/#{hSection.sSlug}"
    sTitle = tmpl.fa_icon(hSection.sIcon)+hSection.sTitle
    tmpl.li "","", [
      tmpl.a sHref,"",sTitle
    ]

