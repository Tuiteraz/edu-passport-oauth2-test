define [
  "domReady"
  "helpers/helpers"
  'beaver-console'
  'libs/beaver/helpers'
], (domReady,hlprs,jconsole) ->
  jconsole.info "stylesheets.pack"

  domReady =>
    sLogHeader = "stylesheets.pack"

    if is_ie()
      jconsole.info "#{sLogHeader} - loading additional css for IE"
      hlprs.load_stylesheet "../js/libs/font-awesome/css/font-awesome-ie.css"
      hlprs.load_stylesheet "css/stylesheet-ie.css"
      hlprs.load_stylesheet "css/stylesheet-ie8.css" if is_ie 8
      hlprs.load_stylesheet "css/stylesheet-ie9.css" if is_ie 9
    if is_opera()
      jconsole.info "#{sLogHeader} - loading additional css for OPERA"
      hlprs.load_stylesheet "css/stylesheet-opera.css"


