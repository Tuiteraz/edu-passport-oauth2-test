#--( DEPENDENCIES

log          = require("winston-wrapper")(module)
config       = require "nconf"

#--) DEPENDENCIES

module.exports = (req,res)->
  req.logOut()
  res.redirect '/'
