#--( DEPENDENCIES

log          = require("winston-wrapper")(module)
config       = require "nconf"

passport     = require 'passport'

#--) DEPENDENCIES

module.exports = (req,res,next)->
  log.info 'someone trying to login'
  passport.authenticate('local', (err,user,info)->
    log.info 'username:', user.username
    if err
      next err
    else
      if user
        req.logIn user, (err)->
          if err
            next err
          else
            if !req.session.returnTo
              log.info "No return to path, so redirecting to /private"
              res.redirect '/private'
            else
              log.info "It seems, that it's oauth2 req so will redirect to : #{req.session.returnTo} "
              res.redirect req.session.returnTo
      else
        req.flash 'error','Auth error.Try again'
        res.redirect '/login'
  )(req,res,next)