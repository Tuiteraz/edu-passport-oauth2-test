#--( DEPENDENCIES

log            = require('winston-wrapper')(module)
config         = require('nconf')

User           = require('mongoose').model 'user'

#--) DEPENDENCIES

module.exports = (req,res,next) ->
  user = new User { username: req.body.email, password : req.body.password }
  user.save (err)->
    if err
      next err
    else
      req.login user, (err)->
        if err
          next err
        else
          res.redirect '/private'