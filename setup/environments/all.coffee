#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
config         = require('nconf')

express        = require 'express'
expressLogger  = require '../../middlewares/express-logger'
expressFlash   = require 'express-flash'

#staticFavicon  = require 'static-favicon'
#bodyParser     = require 'body-parser'
#cookieParser   = require 'cookie-parser'
#session        = require 'express-session'
#methodOverride = require 'method-override'
#errorHandler   = require 'errorhandler'
serveStatic    = require 'serve-static'

passport       = require 'passport'
path           = require 'path'

#--) DEPENDENCIES

rootDir = process.cwd()

module.exports = ()->
  @set 'views', path.join(rootDir,"views")
  @set 'view engine', 'jade'
  @use express.favicon()
  @use expressLogger
  @use express.cookieParser()
  @use express.bodyParser()
  @use express.session({secret:"carpe diem"})
  @use express.methodOverride()
  @use expressFlash()
  @use passport.initialize()
  @use passport.session()
  @use @router
  @use serveStatic( 'public' )
  @use express.errorHandler()

