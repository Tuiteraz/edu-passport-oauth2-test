// Generated by CoffeeScript 1.6.3
(function() {
  var config, express, expressFlash, expressLogger, log, passport, path, rootDir, serveStatic;

  log = require('winston-wrapper')(module);

  config = require('nconf');

  express = require('express');

  expressLogger = require('../../middlewares/express-logger');

  expressFlash = require('express-flash');

  serveStatic = require('serve-static');

  passport = require('passport');

  path = require('path');

  rootDir = process.cwd();

  module.exports = function() {
    this.set('views', path.join(rootDir, "views"));
    this.set('view engine', 'jade');
    this.use(express.favicon());
    this.use(expressLogger);
    this.use(express.cookieParser());
    this.use(express.bodyParser());
    this.use(express.session({
      secret: "carpe diem"
    }));
    this.use(express.methodOverride());
    this.use(expressFlash());
    this.use(passport.initialize());
    this.use(passport.session());
    this.use(this.router);
    this.use(serveStatic('public'));
    return this.use(express.errorHandler());
  };

}).call(this);

/*
//@ sourceMappingURL=all.map
*/
