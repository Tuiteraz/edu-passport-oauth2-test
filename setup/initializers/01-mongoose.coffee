#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
config         = require('nconf')
Colors         = require('colors')

mongoose       = require('mongoose')
requireTree    = require('require-tree')

models         = requireTree('../../models/')


#--) DEPENDENCIES

module.exports = (callback)->

  options =
    server:
      socketOptions :  { keepAlive: 1 }
    replset:
      socketOptions:  { keepAlive: 1 }

  mongoose.connection.on 'open', ()->
    log.info('Connected to mongo server!'.green);
    return callback()

  mongoose.connection.on 'error', (err)->
    log.error 'Could not connect to mongo server!'
    log.error err.message
    callback()
    return err

  try
    mongoose.connect config.get('mongoose:db'),options
#    mongoose.connection
    log.info "Started connection on "+(config.get('mongoose:db')).cyan + ", waiting for it to open...".grey
  catch err
    log.error ('Setting up failed to connect to ' + config.get('mongoose:db')).red, err.message
