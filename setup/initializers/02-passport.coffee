#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
config         = require('nconf')

passport       = require "passport"
LocalStrategy  = require("passport-local").Strategy
BasicStrategy  = require("passport-http").BasicStrategy
ClientPasswordStrategy  = require("passport-oauth2-client-password").Strategy
BearerStrategy = require("passport-http-bearer").Strategy

mongoose       = require('mongoose')
User           = mongoose.model 'user'
AccessToken    = mongoose.model 'access_token'
RefreshToken   = mongoose.model 'refresh_token'
Client         = require "../../models/client"

#--) DEPENDENCIES

module.exports = ()->

  # LocalStrategy
  #---------------
  # This strategy is used to authenticate users based on a username and password.
  # Anytime a request is made to authorize an application, we must ensure that
  # a user is logged in before asking them to approve the request.
  passport.use new LocalStrategy {
    usernameField : 'email'
    passwordField : 'password'
  }, (username,password, callback)->
    User.findOne { username : username }, (err,user)->
      if err
        callback err
      else
        if user
          user.comparePassword password, (err,isMatch)->
            callback(err) if err
            if isMatch
              callback null,user
            else
              callback null,false, {message:"Incorrect password."}
        else
          callback null,false, {message:"Incorrect username."}

  passport.serializeUser (user,callback)->
    callback null, user.id

  passport.deserializeUser (id,callback)->
    User.findById id, (err,user)->
      if err
        callback err
      else
        callback null, user


  # ClientPasswordStrategy
  #------------------------
  # These strategies are used to authenticate registered OAuth clients.  They are
  # employed to protect the `token` endpoint, which consumers use to obtain
  # access tokens.  The OAuth 2.0 specification suggests that clients use the
  # HTTP Basic scheme to authenticate.  Use of the client password strategy
  # allows clients to send the same credentials in the request body (as opposed
  # to the `Authorization` header).  While this approach is not recommended by
  # the specification, in practice it is quite common.
  passport.use new ClientPasswordStrategy (clientId,clientSecret,done)->
    Client.findByClientId clientId, (err,client)->
      return done(err) if err
      return done(null,false) if !client
      return done(null,false) if client.cleintSecret != clientSecret
      return done(null,client)

  # BearerStrategy
  #----------------
  # This strategy is used to authenticate users based on an access token (aka a
  # bearer token).  The user must have previously authorized a client
  # application, which is issued an access token to make requests on behalf of
  # the authorizing user.
  passport.use new BearerStrategy (accessToken, done)->
    AccessToken.findOne {token:accessToken}, (err,token)->
      return done(err) if err
      return done(null,false) if !token

      livedTime = (Date.now() - token.created)/1000
      if Math.round(livedTime) > config.get("security:tokenLife")
        AccessToken.remove {token:accessToken}, (err)->
          return done(err) if err

        done null, false, {message:'Token expired'}

      User.findById token.userId, (err,user)->
        return done(err) if err
        return done(null,false,{message:'Unknown user'}) if !user
        info =
          scope : "*"

        done null,user,info