#--( DEPENDENCIES
log            = require('winston-wrapper')(module)
colors         = require('colors')
#--) DEPENDENCIES

module.exports = (req,res, next)->
  date = new Date()
  hh = date.getHours()
  mm = date.getMinutes()
  time = "#{hh}:#{mm}"

  log.info time, "[#{req.method.grey}]:", req.url
  next()