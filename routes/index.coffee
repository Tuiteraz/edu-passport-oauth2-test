#--( DEPENDENCIES

log          = require("winston-wrapper")(module)
config       = require "nconf"
requireTree  = require 'require-tree'

controllers         = requireTree '../controllers'
mustAuthenticatedMw = require '../middlewares/must-authenticated'

#--) DEPENDENCIES

module.exports = ()->
  # only for registered users
  @all "/private",   mustAuthenticatedMw
  @all "/private/*", mustAuthenticatedMw

  # basic routes
  @get "/",          controllers.render 'public'
  @get "/private",   controllers.render 'private'
  @get "/fail",      controllers.render 'fail'

  # auth controllers
  @get  "/login",    controllers.render "login"
  @post "/login",    controllers.users.login
  @get  "/register", controllers.render "register"
  @post "/register", controllers.users.register
  @get  "/logout",   controllers.users.logout

  # api
  @get  "/ifttt/v1/oauth2/authorize", controllers.oauth2.authorization
  @post "/ifttt/v1/oauth2/authorize/decision", controllers.oauth2.decision
  @post "/api/oauth2/token", controllers.oauth2.token

  # error handling
  # BUG - if enabled prevents public resources to serve -
#  @get  "*",         controllers.render '404'