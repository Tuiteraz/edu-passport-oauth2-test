#--( DEPENDENCIES
_           = require "underscore"
_.str       = require "underscore.string"
_.mixin _.str.exports()
Colors      = require "colors"

fs          = require 'fs'
fsExtra     = require 'fs-extra'
url         = require 'url'
util        = require 'util'
path        = require 'path'
_eval       = require 'eval'
admZip      = require 'adm-zip'
async       = require 'async'

mongoose        = require 'mongoose'

hlprs       = require "./../helpers/common-helpers"
fileHlprs   = require "./../helpers/file-helpers"

#--) DEPENDENCIES

#--- BODY --------------

#+2013.10.31 tuiteraz
createConnection = (dbOptions, callback) ->
  logHeader = "[mongoose-connection]"
  connection = null
  if _.size(mongoose.connections) > 0
    _.each mongoose.connections, (mCurrConn)->
      if mCurrConn.name == dbOptions.sName
        connection = mCurrConn

  if !connection
    hOptions =
      server:
        socketOptions :  { keepAlive: 1 }
      replset:
        socketOptions:  { keepAlive: 1 }

    connection = mongoose.createConnection "mongodb://#{dbOptions.user}:#{dbOptions.password}@localhost/#{dbOptions.name}",hOptions
    connection.on 'error', console.error.bind(console,'mongoose connection error:')
    connection.once 'open', ->
      console.log "#{logHeader} mongoose connected to #{connection.name}!"
      callback(connection)
  else
    console.log "#{logHeader} Using existing connection to #{connection.name}!"
    callback(connection)

  return connection

#+2013.10.31 tuiteraz
createModel = (collectionName,connection,req,callback) ->
  logHeader = "[mongoose-model]"
  if _.isNull req
    sSiteSchemaspath = Assetshlprs.lightbeam_schemas_path()
  else if SessSiteDetails.attr_of(req,'bIsLightbeamCP')
    sSiteSchemaspath = Assetshlprs.lightbeam_schemas_path()
  else
    sSiteSchemaspath = Assetshlprs.schemas_path SessSiteDetails.attr_of(req,'sSourcepath')

  sSchemaFilepath = path.join sSiteSchemaspath, "#{collectionName}.js"
  sSchemaMethodsFilepath = path.join sSiteSchemaspath, "#{collectionName}-methods.js"
  sSchemaMiddlewareMethodsFilepath = path.join sSiteSchemaspath, "#{collectionName}-middleware-methods.js"

  async.series [
    (next)->
      if collectionName == "file"
        # we need to check is there file.js or we using ref_file.js
        # if we use ref_file.js then we need to change sSchemaFilepath : file -> ref_file
        fs.exists sSchemaFilepath, (bExists) ->
          if bExists
            next()
          else
            collectionName = "ref_#{collectionName}"
            sSchemaFilepath = path.join sSiteSchemaspath, "#{collectionName}.js"
            next()
      else
        next()
  ], ->
    if _.isFunction connection.models[collectionName]
      console.log "#{logHeader} using precompiled model [#{collectionName}]"
      callback null, connection.models[collectionName]
    else
      console.log "#{logHeader} creating model [#{collectionName}] for scheme [#{sSchemaFilepath}]"

      fs.exists sSchemaFilepath, (bExists) ->
        if bExists
          fs.readFile sSchemaFilepath, (jErr,jData) ->
            throw jErr if jErr
            #console.log "reading file: #{sSchemaFilepath}"
            try
              hSchema = JSON.parse jData.toString()
            catch e
              return callback "#{collectionName}.js : #{e}",null

            async.series [
              # load schema methods
              (next)->
                console.log "#{logHeader}[async-step] loading schema methods..."
                fs.exists sSchemaMethodsFilepath, (bExists) ->
                  if bExists
                    fs.readFile sSchemaMethodsFilepath, (sErr,jMethodsTxt)->
                      hMethods = _eval jMethodsTxt.toString(),true
                      next null, hMethods
                  else
                    next null,null

              # load middleware schema methods
              (next)->
                console.log "#{logHeader}[async-step] loading middleware schema methods..."
                fs.exists sSchemaMiddlewareMethodsFilepath, (bExists) ->
                  if bExists
                    fs.readFile sSchemaMiddlewareMethodsFilepath, (sErr,jMdlwrMethodsTxt)->
                      hMethods = _eval jMdlwrMethodsTxt.toString(),true
                      next null, hMethods
                  else
                    next null,null
              # load referenced shemas & compiling models
              (next)->
                aRefs = hlprs.get_referenced_schemas(hSchema)
                if _.size(aRefs)>0
                  console.log "#{logHeader}[async-step] [#{collectionName}] compile refs schemas: #{JSON.stringify(aRefs)}"

                  _.each aRefs, (sRefschemaName,iIdx) ->
                    return if _.isFunction connection.models[sRefschemaName]

                    sSchemaFilepath = path.join sSiteSchemaspath, "#{sRefschemaName}.js"
                    jData = fs.readFileSync sSchemaFilepath
                    try
                    #console.log jData.toString()
                      hRefschema = JSON.parse jData.toString()
                    catch e
                      console.log "#{sRefschemaName}.js : #{e}"
                      return next "#{sRefschemaName}.js : #{e}",null

                    try
                      mRefschema = new mongoose.Schema hRefschema
                      mRefModel = connection.model sRefschemaName,mRefschema
                      aRefs[iIdx] =
                        sName: sRefschemaName
                        mSchema: mRefschema
                        mModel : mRefModel
                    catch e
                      console.log "#{sRefschemaName}.js : #{e}"
                      return next "#{sRefschemaName}.js : #{e}",null

                  return next null,aRefs
                else
                  next null,true

            ], (Err,aRes)->
              console.error "#{logHeader} #{JSON.stringify(Err)} " if Err
              return callback "#{sRefschemaName}.js : #{Err.message}",null if Err

              # aRes[0] = schema methods
              # aRes[1] = middleware schema methods
              # aRes[2] = array of ref's
              hMethods     = aRes[0]
              hMdwrMethods = aRes[1]
              aRefs        = aRes[2]

              try
                mSchema = new mongoose.Schema hSchema
              catch e
                return callback "#{collectionName}.js : #{e}",null

              if _.size(hMethods) > 0
                for sName, fnMethod of hMethods
                  mSchema.method sName, fnMethod

              if _.size(hMdwrMethods) > 0
                if hMdwrMethods.pre
                  if _.isObject hMdwrMethods.pre
                    _.each hMdwrMethods.pre, (fn,sEventName) ->
                      mSchema.pre sEventName, fn
                  else
                    console.error "#{logHeader} hMdwrMethods.pre isn't object"
                if hMdwrMethods.post
                  if _.isObject hMdwrMethods.post
                    _.each hMdwrMethods.post, (fn,sEventName) ->
                      mSchema.post sEventName, fn
                  else
                    console.error "#{logHeader} hMdwrMethods.post isn't object"

              mModel = connection.model collectionName,mSchema
              callback null, mModel
        else
          sMessage = "#{logHeader} #{sSchemaFilepath} does't exists"
          return callback sMessage.bold.red,null


#+2013.10.31 tuiteraz
dispatch = (jResponse, hParams)->
  logHeader = "[DB]-dispatch():"
  console.log "#{logHeader}"

  sRequest = decodeURIComponent(hParams.sUri)

  rexpGetAdmin = new RegExp "^\/db$","i"
  rexpNew      = new RegExp "^\/db\/([a-z\d_]+)\/new$","i"
  rexpCreate   = new RegExp "^\/db\/([a-z\d_]+)\/create$","i"
  rexpUpload   = new RegExp "^\/db\/([a-z\d_]+)\/upload$","i"

  bIsGetAdmin = rexpGetAdmin.test sRequest
  bIsGet      = /^\/db\/([a-z\d_]+)\/\?([a-z0-9\w\W]+)$/i.test hParams.surl
  bIsGetFile  = /^\/db\/([a-z\d_]+)\/([a-z0-9]+)$/i.test hParams.surl
  bIsUpload   = /^\/db\/([a-z\d_]+)\/upload$/i.test hParams.surl
  bIsNew      = rexpNew.test sRequest

  if hParams.hRequest.body
    hParams.hRequest.body.hQuery   ||= "{}"
    hParams.hRequest.body.hData    ||= "{}"
    hParams.hRequest.body.hOptions ||= "{}"

  if hParams.sRequestMethod == "GET"
    if bIsGetAdmin
      hlprs.write_response 200, "db admin - pending",jResponse, false

    else if bIsGet
      hParams.aDbReqMatches = hParams.surl.match /^\/db\/([a-z\d_]+)\/\?([a-z0-9\w\W]+)$/i
      db_get(jResponse, hParams)  # GET /db/:collection

    else if bIsGetFile
      hParams.aDbReqMatches = hParams.surl.match /^\/db\/([a-z\d_]+)\/([a-z0-9]+)$/i
      db_get_file(jResponse,hParams) # GET /db/file/:id

    else if bIsNew
      hParams.aDbReqMatches = sRequest.match rexpNew
      db_new(jResponse, hParams)  # GET /db/:collection/new

    else
      console.log "[ERROR] #{sRequest}"
      hlprs.write_response 500, {sMessage:"#{hParams.sRequestMethod} #{sRequest}"},jResponse

  else if hParams.sRequestMethod == "POST"
    if !bIsUpload
      hParams.aDbReqMatches = sRequest.match rexpCreate
      # POST /db/:collection/create
      db_create(jResponse, hParams)
    else
      hParams.aDbReqMatches = sRequest.match rexpUpload
      # POST /db/:collection/upload
      db_upload(jResponse, hParams)

  else if hParams.sRequestMethod == "PUT"
    # PUT /db/:collection/?hQuery={}&hData={}&hOptions={}
    hParams.aDbReqMatches = hParams.surl.match /^\/db\/([a-z\d_]+)\/$/i
    db_update(jResponse, hParams)

  else if hParams.sRequestMethod == "DELETE"
    hSecurity = SessSiteDetails.attr_of(hParams.hRequest,'hSecurity')
    if !hlprs.is_cp_request(hParams.hRequest) && hSecurity.bDisableClientRemoveRequest
      hlprs.write_response 523, {sMessage:"#{hParams.sRequestMethod} is not allowed for client app" },jResponse
    else
      hParams.aDbReqMatches = hParams.surl.match /^\/db\/([a-z\d_]+)\/\?([a-z0-9\w\W]+)/i

      # DELETE /db/:collection/:id
      # DELETE /db/:collection/?hQuery={}
      db_remove(jResponse, hParams)

  else
    console.log "[ERROR] #{sErr}"
    hlprs.write_response 500, {sMessage:"#{hParams.sRequestMethod} #{sRequest}"},jResponse



# POST /db/:collection
#+2013.10.31 tuiteraz
db_create = (jResponse, hParams) ->

  console.dir hParams.aDbReqMatches

  dbOptions = SessSiteDetails.attr_of(hParams.hRequest,'hMongoDB')
  collectionName = hParams.aDbReqMatches[1]

  logHeader = "[DB] #{collectionName}.create():"

  console.log "#{logHeader} creating connection"
  createConnection dbOptions, (connection)->
    console.log "#{logHeader} creating model"
    createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
      if sErr
        console.error "[ERROR] #{logHeader} #{sErr}"
        hlprs.write_response 500,{sMessage:sErr},jResponse

      else
        console.log "#{logHeader} creating record"
        mModel.create hParams.hRequest.body, (Err, mdRecord) ->
          if Err
            console.error "[ERROR] #{logHeader} #{Err}"
            hlprs.write_response 500,{sMessage:Err.message},jResponse
          else
            console.dir mdRecord
            hlprs.write_response 200,mdRecord,jResponse


# GET /db/:collection/?hQuery={}&hParams={}
#+2013.10.31 tuiteraz
db_get = (jResponse, hParams) ->


  dbOptions = SessSiteDetails.attr_of(hParams.hRequest,'hMongoDB')
  collectionName = hParams.aDbReqMatches[1]

  logHeader = "[DB] #{collectionName}.get():"

  try
    hQuery  = JSON.parse hParams.hQuery.hQuery
    hQuery ||= {}
    hOptions = JSON.parse hParams.hQuery.hOptions
    hOptions ||={}
  catch e
    sMsg = "#{logHeader} check hQuery and hParams request options: #{e.message}"
    hlprs.write_response 500, {sMessage:sMsg}, jResponse
    return


  console.dir hParams.aDbReqMatches

  console.log "#{logHeader} creating connection"
  createConnection dbOptions, (connection)->
    console.log "#{logHeader} creating model"
    createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
      if sErr
        console.error "[ERROR]#{logHeader} #{sErr}"
        hlprs.write_response 500, {sMessage:sErr}, jResponse

      else
        console.log "#{logHeader} query: #{JSON.stringify(hQuery)}"
        console.log "#{logHeader} options: #{JSON.stringify(hOptions)}"

        console.log "#{logHeader} getting records"
        if hOptions.sPopulateProperties
          sPopulateProperties = hOptions.sPopulateProperties
        else
          sPopulateProperties = ""

        if hOptions.sPopulateModels
          sPopulateModels = hOptions.sPopulateModels
        else
          sPopulateModels = ""

        if hOptions.sSort
          sSort = hOptions.sSort
        else
          sSort = ""

        mQuery = mModel.find(hQuery).sort(sSort).populate(sPopulateProperties,"-jBuffer",sPopulateModels)

        mQuery.exec (Err, mRes) ->
          if Err
            console.error "[ERROR]#{logHeader} #{Err.message}"
            hlprs.write_response 500, {sMessage : Err.message}, jResponse

          else if _.isArray mRes
            console.log "#{logHeader} found #{_.size(mRes)} records"
            if hOptions.bToZip
              console.log "#{logHeader} preparing to send zip"

              sZpath = path.join process.cwd(),"tmp","#{collectionName}.zip"

              spath = path.join process.cwd(),"tmp","#{collectionName}.json"
              jStream = fs.createWriteStream spath
              jStream.once 'open', (fd) ->
                jStream.write JSON.stringify(mRes)
                jStream.end ->
                  zip = new admZip()
                  zip.addLocalFile spath
                  async.series [
                    (callback)->
                      zip.writeZip sZpath
                      console.log "#{logHeader} zip ready"
                      callback null,true

                    (callback)->
                      jZStream = fs.createReadStream sZpath
                      jResponse.writeHead 200, {
                        "Content-Type": "application/zip"
                        "Content-Disposition":"attachment; filename=#{collectionName}.zip"
                        "Set-Cookie": "fileDownload=true; path=/"
                      }
                      jZStream.on 'end', ->
                        console.log "#{logHeader} zip send. Clearing tmp files..."
                        fs.unlink spath
                        fs.unlink sZpath
                        callback null,true
                      jZStream.pipe jResponse
                  ]
            else
              hlprs.write_response 200, mRes, jResponse

# GET /db/:collection/:id
#+2013.12.2 tuiteraz
db_get_file = (jResponse, hParams) ->
  dbOptions = SessSiteDetails.attr_of(hParams.hRequest,'hMongoDB')
  collectionName = hParams.aDbReqMatches[1]
  sFileId         = hParams.aDbReqMatches[2]
  hQuery          = {_id:sFileId}

  logHeader = "[DB] #{collectionName}.get_file(#{sFileId}):"

  console.log "#{logHeader} creating connection"
  createConnection dbOptions, (connection)->
    console.log "#{logHeader} creating model"
    createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
      if sErr
        console.error "[ERROR]#{logHeader} #{sErr}"
        hlprs.write_response 500, {sMessage: sErr}, jResponse

      else
        console.log "#{logHeader} looking for file"
        mQuery = mModel.findOne(hQuery)
        mQuery.exec (Err, mRes) ->
          if Err
            console.error "[ERROR]#{logHeader} #{Err.message}"
            hlprs.write_response 500, {sMessage:Err.message}, jResponse
          else
            if !_.isUndefined(mRes) && !_.isNull(mRes)
              hHead =
                "Content-Type": mRes.sMime
                "Content-Disposition":"attachment; filename=#{mRes.sTitle}"
#                "Set-Cookie": "fileDownload=true; path=/"

              if !_.isUndefined(mRes.sFullpath) && _.isUndefined(mRes.jBuffer)
                sSysFilepath = path.join Assetshlprs.base_path(), hParams.sSourcepath,mRes.sFullpath
                fs.exists sSysFilepath, (bExists) ->
                  if bExists
                    jResponse.writeHead 200, hHead
                    console.log "#{logHeader} serving file [#{mRes.sTitle}] as [#{mRes.sMime}]"
                    fsStream = fs.createReadStream sSysFilepath
                    fsStream.pipe jResponse
                  else
                    console.error "#{logHeader} doesn't exist: #{sSysFilepath}"
                    hlprs.write_response 404, {sMessage:sSysFilepath}, jResponse
              else if !_.isUndefined(mRes.jBuffer)
                jResponse.writeHead 200, hHead
                console.log "#{logHeader} serving file [#{mRes.sTitle}] as [#{mRes.sMime}]"
                jResponse.end mRes.jBuffer

              else
                console.error "#{logHeader} sFullpath & jBuffer is undefined for : #{JSON.stringify(hQuery)}"
                hlprs.write_response 404, {sMessage:"sFullpath & jBuffer is undefined for : #{JSON.stringify(hQuery)}"}, jResponse

            else
              console.error "#{logHeader} doesn't exist: #{JSON.stringify(hQuery)}"
              hlprs.write_response 404, {sMessage:"not found #{JSON.stringify(hQuery)}"}, jResponse







# GET /db/:collection/new
#+2013.10.31 tuiteraz
db_new = (jResponse, hParams) ->
  dbOptions = SessSiteDetails.attr_of(hParams.hRequest,'hMongoDB')
  collectionName = hParams.aDbReqMatches[1]

  logHeader = "[DB] #{collectionName}.new():"
  console.dir hParams.aDbReqMatches

  console.log "#{logHeader} creating connection"
  createConnection dbOptions, (connection)->
    console.log "#{logHeader} creating model"
    createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
      if sErr
        console.error "[ERROR]#{logHeader} #{sErr}"
        hlprs.write_response 500, {sMessage:sErr}, jResponse

      else
        console.log "#{logHeader} new record"
        mdNew = new mModel()

        console.dir mdNew

        hlprs.write_response 200, mdNew, jResponse


# DELETE /db/:collection/:id
#+2013.10.31 tuiteraz
db_remove = (jResponse, hParams) ->
  console.dir hParams.aDbReqMatches

  dbOptions = SessSiteDetails.attr_of(hParams.hRequest,'hMongoDB')
  collectionName = hParams.aDbReqMatches[1]

  hQuery  = JSON.parse hParams.hQuery.hQuery
  hOptions = JSON.parse hParams.hQuery.hOptions

  logHeader = "[DB] #{collectionName}.remove():"

  console.log "#{logHeader} creating connection"
  createConnection dbOptions, (connection)->
    console.log "#{logHeader} creating model"
    createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
      if sErr
        console.error "[ERROR]#{logHeader} #{sErr}"
        hlprs.write_response 500, {sMessage:sErr}, jResponse
      else
        if _.isEmpty(hQuery)
          sErr = "query can't be empty"
          console.error "[ERROR]#{logHeader} #{sErr}"
          hlprs.write_response 500, {sMessage:sErr} , jResponse
        else
          console.log "#{logHeader} remove record by: #{JSON.stringify(hQuery)}"

          async.series [
            (next)->
              if collectionName =='file' || collectionName =='ref_file'
                console.log "#{logHeader}[async-step] removing file..."
                mQuery = mModel.find hQuery
                mQuery.exec (Err,mFile) ->
                  if Err
                    next Err.message,true
                  else
                    if _.isArray mFile
                      _.each mFile, (mRec)->
                        if mRec.sFullpath
                          fs.unlink path.join(Assetshlprs.base_path(),hParams.sSourcepath,mRec.sFullpath)
                      next null,true
                    else
                      if mFile.sFullpath
                        fs.unlink path.join(Assetshlprs.base_path(),hParams.sSourcepath,mFile.sFullpath)
                      next null,true
              else
                next null,true

            (next) ->
              console.log "#{logHeader}[async-step] removing mongo doc..."
              mQuery = mModel.remove hQuery
              mQuery.exec (Err) ->
                if Err
                  next Err.message,true
                else
                  console.log "#{logHeader} removing done"
                  hlprs.write_response 200, {sMessage : "removing done"}, jResponse
                  next null,true
          ], (sErr,aRes) ->
            if sErr
              console.error "[ERROR]#{logHeader} #{sErr}"
              hlprs.write_response 500, {sMessage : sErr}, jResponse



# PUT /db/:collection/?hQuery={}&hData={}&hOptions={}
#+2013.11.9 tuiteraz
db_update = (jResponse, hParams) ->


  dbOptions = SessSiteDetails.attr_of(hParams.hRequest,'hMongoDB')
  collectionName = hParams.aDbReqMatches[1]

  logHeader = "[DB] #{collectionName}.update():"

  try
    hQuery   = JSON.parse hParams.hRequest.body.hQuery
    hData    = JSON.parse hParams.hRequest.body.hData
    hOptions = JSON.parse hParams.hRequest.body.hOptions
    hQuery   ||= {}
    hData    ||= {}
    hOptions ||= {}
  catch e
    sMsg = "#{logHeader} check hQuery and hParams request options: #{e.message}"
    hlprs.write_response 500, {sMessage:sMsg}, jResponse
    return

  if _.isString hOptions.aArrayProperties
    hOptions.aArrayProperties = [hOptions.aArrayProperties]

  hOptions.upsert = hOptions.bUpsert
  delete hOptions.bUpsert

  console.log "#{logHeader} creating connection"
  createConnection dbOptions, (connection)->
    console.log "#{logHeader} creating model"
    createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
      if sErr
        console.error "[ERROR]#{logHeader} #{sErr}"
        hlprs.write_response 500, {sMessage: sErr}, jResponse

      else
        if _.isEmpty(hQuery) || _.isEmpty(hData)
          sErr = "hQuery and hData can't be empty"
          console.error "[ERROR]#{logHeader} #{sErr}"
          hlprs.write_response 500, {sMessage:sErr}, jResponse

        else
          console.log "#{logHeader} update record by: #{JSON.stringify(hQuery)}"
          console.log "#{logHeader} with data: #{JSON.stringify(hData)}"
          console.log "#{logHeader} with params: #{JSON.stringify(hOptions)}"

          if hOptions.aArrayProperties
            hArrayData = _.clone hData
            delete hData[sArrayProperty] for sArrayProperty in hOptions.aArrayProperties
            mModel.findOne hQuery, (sErr,mdRecord)->
              if mdRecord
                hArrayData = update_db_record_array_properties(hArrayData,mdRecord,hOptions,jResponse)
                mdRecord.save()
              else
                sErr = "can't find record for hQuery: #{JSON.stringify(hQuery)}"
                console.error "[ERROR]#{logHeader} #{sErr}"

          if hOptions.aI18nProperties && _.isArray(hOptions.aI18nProperties)
            hI18nData = _.clone hData
            delete hData[sI18nProperty] for sI18nProperty in hOptions.aI18nProperties
            mModel.findOne hQuery, (sErr,mdRecord)->
              if mdRecord
                update_db_record_i18n_properties(hI18nData,mdRecord,hOptions)
                mdRecord.save()
              else
                sErr = "can't find record for hQuery: #{JSON.stringify(hQuery)}"
                console.error "[ERROR]#{logHeader} #{sErr}"

          hData.dtModified = Date()
          mQuery = mModel.update hQuery,hData,hOptions

          mQuery.exec (Err, iNumberAffected) ->
            if Err & (iNumberAffected==0)
              console.error "[ERROR]#{logHeader} #{Err.message}"
              hlprs.write_response 500,{sMessage: Err.message}, jResponse

            else
              console.log "#{logHeader} updating done, records affected: #{iNumberAffected} "

              hRes =
                iNumberAffected:iNumberAffected

              if !_.isUndefined(hArrayData)
                hRes["hArrayData"] = hArrayData

              if !jResponse.bIsSent & !_.isUndefined(iNumberAffected)
                hlprs.write_response 200, hRes , jResponse
              else
                hlprs.write_response 500, {sMessage: "iNumberAffected = undefined. Check logs."} , jResponse

#+2014.1.3 tuiteraz
update_db_record_array_properties=(hArrayData,mdRecord,hOptions,jResponse)->
  logHeader = "[DB] update_db_record_array_properties():"
  for sArrayProperty in hOptions.aArrayProperties
    if hOptions.sArrayPropertiesOperation
      sArrayPropertiesOperation = hOptions.sArrayPropertiesOperation
    else
      sArrayPropertiesOperation = "push"

    if sArrayPropertiesOperation == "push"
      console.log "#{logHeader} updating property: #{sArrayProperty}"
      if _.size(hArrayData[sArrayProperty])==1 || !_.isArray(hArrayData[sArrayProperty])
        if _.size(hArrayData[sArrayProperty])==1 && _.isArray(hArrayData[sArrayProperty])
          Data = hArrayData[sArrayProperty][0]
        else if _.isObject(hArrayData[sArrayProperty])
          Data = hArrayData[sArrayProperty]

        console.log "#{logHeader} pushing #{JSON.stringify(Data)}"
        mdRecord[sArrayProperty].push Data

        hLast = _.last(mdRecord[sArrayProperty])
        Data._id = hLast._id if !_.isUndefined(hLast._id)
      else
        if _.isArray hArrayData[sArrayProperty]
          Data = hArrayData[sArrayProperty]
          for hEl in Data
            console.log "#{logHeader} pushing #{JSON.stringify(hEl)}"
            mdRecord[sArrayProperty].push hEl

            hLast = _.last(mdRecord[sArrayProperty])
            hEl._id = hLast._id if !_.isUndefined(hLast._id)
        else
          console.error "#{logHeader} #{sArrayProperty} not found in recieved data"

      return Data

    else if sArrayPropertiesOperation == "remove"
      if _.size(hArrayData[sArrayProperty])==1 || !_.isArray(hArrayData[sArrayProperty])
        if _.size(hArrayData[sArrayProperty])==1 && _.isArray(hArrayData[sArrayProperty])
          Data = hArrayData[sArrayProperty][0]
        else if _.isObject(hArrayData[sArrayProperty])
          Data = hArrayData[sArrayProperty]

        if _.isObject Data
          # Data must have key "_id"
          Data._id ||= Data.id if _.has(Data,"id")
          hEl = _.findWhere mdRecord[sArrayProperty], {id:Data._id}
          iIdx = _.indexOf mdRecord[sArrayProperty], hEl
        else
          iIdx = mdRecord[sArrayProperty].indexOf Data
        console.log "#{logHeader} removing [#{JSON.stringify(Data)}] at index [#{iIdx}]"
        mdRecord[sArrayProperty].splice(iIdx,1) if iIdx>=0
      else
        if _.isArray hArrayData[sArrayProperty]
          for Data in hArrayData[sArrayProperty]
            iIdx = mdRecord[sArrayProperty].indexOf Data
            console.log "#{logHeader} removing [#{JSON.stringify(Data)}] at index [#{iIdx}]"
            mdRecord[sArrayProperty].splice(iIdx,1) if iIdx>=0
        else
          console.error "#{logHeader} #{sArrayProperty} not found in recieved data"

    else if sArrayPropertiesOperation == "update"  # one array property element by request
      if !hOptions.hArrayPropertyQuery
        sErr = "no hOptions.hArrayPropertyQuery found"
        console.error "[ERROR]#{logHeader} #{sErr}"
        hlprs.write_response 500, {sMessage : sErr} , jResponse
        jResponse.bIsSent = true
        return

      if _.size(hArrayData[sArrayProperty])==1 || _.isObject(hArrayData[sArrayProperty])
        if _.size(hArrayData[sArrayProperty])==1 && _.isArray(hArrayData[sArrayProperty])
          Data = hArrayData[sArrayProperty][0]
        else if _.isObject(hArrayData[sArrayProperty])
          Data = hArrayData[sArrayProperty]

        ArrPropertyEl = _.findWhere mdRecord[sArrayProperty], hOptions.hArrayPropertyQuery
        iArrPropertyElIdx = _.indexOf mdRecord[sArrayProperty], ArrPropertyEl

        if !(iArrPropertyElIdx>=0)
          sErr = "can't find arr property element for query:#{JSON.stringify(hOptions.hArrayPropertyQuery)}"
          console.error "[ERROR]#{logHeader} #{sErr}"
          hlprs.write_response 500, {sMessage : sErr} , jResponse
          jResponse.bIsSent = true
          return

        for sPropName, PropValue of Data
          if _.include hOptions.aI18nProperties, sPropName
            mdRecord[sArrayProperty][iArrPropertyElIdx][sPropName][sKey] = sValue for sKey,sValue of PropValue
          else
            mdRecord[sArrayProperty][iArrPropertyElIdx] = PropValue

        mdRecord.markModified sArrayProperty

      else
        console.error "[ERROR]#{logHeader} array property data is array. Must be one item"

#+2014.1.3 tuiteraz
update_db_record_i18n_properties=(hI18nData,mdRecord,hOptions,bMarkModified=true)->
  for sI18nProperty in hOptions.aI18nProperties
    mdRecord[sI18nProperty] ||= {}
    mdRecord[sI18nProperty][sKey] = sValue for sKey,sValue of hI18nData[sI18nProperty]
    mdRecord.markModified sI18nProperty if bMarkModified


# POST /db/:collection/upload
#+2013.10.31 tuiteraz
db_upload = (jResponse, hParams) ->
  console.dir hParams.aDbReqMatches
  dbOptions = SessSiteDetails.attr_of(hParams.hRequest,'hMongoDB')
  collectionName = hParams.aDbReqMatches[1]
  logHeader = "[DB] #{collectionName}.upload():"

  hOptions = hParams.hRequest.body.hOptions if !_.isUndefined(hParams.hRequest.body.hOptions)
  hOptions ||={}
  hOptions.hCreator       ||= null
  hOptions.sUploadDir     ||= ""
  hOptions.bStoreInBuffer ||= false

  if fileHlprs.is_req_body_is_file hParams.hRequest
    aFiles = fileHlprs.parse_body_files hParams.hRequest

    if !hOptions.bStoreInBuffer
      sDestpath = path.join Assetshlprs.base_path(), hParams.sSourcepath,'uploads',hOptions.sUploadDir
      fs.mkdirSync(sDestpath) if !fs.existsSync(sDestpath)

    aFnFiles = []
    iFileCount = _.size aFiles
    aasyncFiles = _.clone aFiles

    if !hOptions.bStoreInBuffer
      for iIdx,hFile of aFiles
        aFnFiles.push (next)->
          hFile = _.clone _.first(aasyncFiles)
          hFile.sSysName = fileHlprs.check_filename_for_dir sDestpath, hFile.sName
          aasyncFiles.splice 0,1

          sSysFilepath = path.join sDestpath, hFile.sSysName
          console.log "#{logHeader} [#{iFileCount-_.size(aasyncFiles)}/#{iFileCount}] writing file: #{sSysFilepath}"

          fs.writeFile sSysFilepath, hFile.jBuffer, (sErr) ->
            if sErr
              console.error "#{logHeader} file copy error: #{sErr}"
              next sErr
            else
              fs.stat sSysFilepath, (sErr,hStats)->
                if sErr
                  console.error "#{logHeader} file stats error: #{sErr}"
                  next sErr

                iSize = hStats.size

                hNewFile =
                  sFullpath : path.join(Settings.mongo.sLocalSiteUploadspath,hOptions.sUploadDir, hFile.sSysName)
                  sTitle    : hFile.sName
                  iSize     : iSize
                  dtCreated : Date()
                  hCreator  : hOptions.hCreator

                console.log "#{logHeader} file uploaded: #{hFile.sName} [ #{iSize} B ]"

                # MongoDB part
                console.log "#{logHeader} creating connection"
                createConnection dbOptions, (connection)->
                  console.log "#{logHeader} creating model"
                  createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
                    if sErr
                      console.error "[ERROR] #{logHeader} #{sErr}"
                      next sErr
                    else
                      console.log "#{logHeader} creating record"
                      mModel.create hNewFile, (sErr, mdRecord) ->
                        if sErr
                          console.error "[ERROR] #{logHeader} #{sErr}"
                          next sErr
                        else
                          console.dir mdRecord
                          next null,mdRecord

    else if hOptions.bStoreInBuffer
      for iIdx,hFile of aFiles
        aFnFiles.push (next)->
          hFile = _.clone _.first(aasyncFiles)
          aasyncFiles.splice 0,1

          iSize = hFile.jBuffer.length

          hNewFile =
            sTitle    : hFile.sName
            iSize     : iSize
            sMime     : hFile.sMime
            jBuffer   : hFile.jBuffer
            dtCreated : Date()
            hCreator  : hOptions.hCreator

          console.log "#{logHeader} file uploaded: #{hFile.sName} [ #{iSize} B ]"

          # MongoDB part
          console.log "#{logHeader} creating connection"
          createConnection dbOptions, (connection)->
            console.log "#{logHeader} creating model"
            createModel collectionName,connection, hParams.hRequest, (sErr,mModel)->
              if sErr
                console.error "[ERROR] #{logHeader} #{sErr}"
                next sErr
              else
                console.log "#{logHeader} creating record"
                mModel.create hNewFile, (sErr, mdRecord) ->
                  if sErr
                    console.error "[ERROR] #{logHeader} #{sErr}"
                    next sErr
                  else
                    console.dir mdRecord
                    hNewFile._id = mdRecord._id
                    delete hNewFile.jBuffer
                    next null,hNewFile

    async.series aFnFiles, (sErr,aRes)->
      if sErr
        hlprs.write_response 500,{sMessage:sErr},jResponse
      else
        hlprs.write_response 200,aRes,jResponse

  else
    hlprs.write_response 500, {sMessage:"file must be uploaded via input.name=FileToUpload"}, jResponse

#------------------------------------------
exports.dispatch          = dispatch
exports.createConnection = createConnection
exports.createModel      = createModel


